package com.vehco.codriver.server.plugin.mobileoffice.config;

import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;
import com.vehco.codriver.server.plugin.mobileoffice.entity.QConnectedTypeEntity;
import com.vehco.codriver.server.plugin.mobileoffice.entity.QConnectedVehiclesEntity;
import com.vehco.codriver.server.plugin.mobileoffice.entity.QVehicleEntity;
import com.vehco.codriver.server.plugin.mobileoffice.entity.VehicleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JpaVehcoQuery<T> extends JPAQuery<T> {
  private final QVehicleEntity vehicleEntity = QVehicleEntity.vehicleEntity;
  private final QConnectedVehiclesEntity connectedVehiclesEntity = QConnectedVehiclesEntity.connectedVehiclesEntity;
  private final QConnectedTypeEntity connectedTypeEntity = QConnectedTypeEntity.connectedTypeEntity;

  @Autowired
  public JpaVehcoQuery(EntityManager em) {
    super(em);
  }

  public <T> JpaVehcoQuery<T> fromVehicles() {
    return (JpaVehcoQuery<T>) from(QVehicleEntity.vehicleEntity);
  }

  public JpaVehcoQuery<T> isActiveVehicle() {
    return (JpaVehcoQuery<T>) where(vehicleEntity.active.isTrue());
  }

  public JpaVehcoQuery<T> fetchJoinConnectedVehicles() {
    return (JpaVehcoQuery<T>) innerJoin(QConnectedVehiclesEntity.connectedVehiclesEntity)
          .on(connectedVehiclesEntity.connectedVehicleid.eq(vehicleEntity.vehicleid)).fetchJoin();
  }

  public JpaVehcoQuery<T> fetchLeftJoinConnectedType() {
    return (JpaVehcoQuery<T>) leftJoin(vehicleEntity.connectedTypeEntity, connectedTypeEntity).fetchJoin();
  }

  public JpaVehcoQuery<T> isConnectedVehicleEndTimestampNull() {
    return (JpaVehcoQuery<T>) where(connectedVehiclesEntity.endTimestamp.isNull());
  }

  public JpaVehcoQuery<T> isNotConnectedVehicleConnectedVehicleId(int vehicleId) {
    return (JpaVehcoQuery<T>) where(connectedVehiclesEntity.connectedVehicleid.ne(vehicleId));
  }

  public JpaVehcoQuery<T> isConnectedVehicleVehicleId(int vehicleId) {
    return (JpaVehcoQuery<T>) where(connectedVehiclesEntity.vehicleid.eq(vehicleId));
  }

  public JpaVehcoQuery<T> areConnectedTypes(List<ConnectedType> connectedTypes) {
    BooleanExpression whereConnectedType = null;
    if (!connectedTypes.isEmpty()) {
      whereConnectedType = connectedTypeEntity.connectedType.in(connectedTypes);
    }

    return (JpaVehcoQuery<T>) where(whereConnectedType);
  }

  public List<VehicleEntity> transformVehicles() {
    return transform(GroupBy.groupBy(vehicleEntity.vehicleid).list(vehicleEntity));
  }
}
