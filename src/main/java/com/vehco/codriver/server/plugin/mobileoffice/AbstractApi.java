package com.vehco.codriver.server.plugin.mobileoffice;


import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;

public abstract class AbstractApi {

  protected final AuthMetaExtractor authMetaExtractor;

  public AbstractApi(AuthMetaExtractor authMetaExtractor) {
    this.authMetaExtractor = authMetaExtractor;
  }

}
