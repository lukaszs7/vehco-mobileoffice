package com.vehco.codriver.server.plugin.mobileoffice.security;


import lombok.Value;

import java.util.Collection;
import java.util.StringJoiner;

@Value
public class AuthMeta {
  Integer customerId;
  Integer userId;
  Collection<Integer> userGroupIds;

  @Override
  public String toString() {
    return new StringJoiner(", ", AuthMeta.class.getSimpleName() + "[", "]")
          .add("customerId=" + customerId)
          .add("userId=" + userId)
          .add("userGroupIds=" + userGroupIds)
          .toString();
  }
}
