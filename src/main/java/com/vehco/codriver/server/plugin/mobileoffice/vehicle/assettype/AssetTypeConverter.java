package com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AssetTypeConverter implements Converter<String, AssetType> {

  @Override
  public AssetType convert(String s) {
    if (isVehicle(s)) {
      return AssetType.VEHICLE;
    } else if (isTempMonitor(s)) {
      return AssetType.TEMP_MONITOR;
    } else if (isCarrierTrailer(s)) {
      return AssetType.CARRIER_TRAILER;
    } else if (isCarrierContainer(s)) {
      return AssetType.CARRIER_CONTAINER;
    } else if (isMinibus(s)) {
      return AssetType.MINIBUS;
    } else if (isVehicleTractorImported(s)) {
      return AssetType.TRACTOR;
    }

    return AssetType.VEHICLE;
  }

  private static Boolean isVehicle(String s) {
    return s == null || s.isEmpty() || s.startsWith("ATX");
  }

  private static boolean isTempMonitor(String s) {
    return s.startsWith("C4A_7A2") || s.startsWith("C4A_4B3");
  }

  private static boolean isCarrierTrailer(String s) {
    List<String> prefixes = Arrays.asList("C4A_7A3", "C4A_4B1", "C4A_4B4", "C4A_4B4-B");
    return prefixes.stream().anyMatch(s::startsWith);
  }

  private static boolean isCarrierContainer(String s) {
    return s.startsWith("C4A_9B6");
  }

  private static boolean isMinibus(String s) {
    return s.startsWith("MunicBox");
  }

  private static boolean isVehicleTractorImported(String s) {
    return s.startsWith("ELO");
  }
}
