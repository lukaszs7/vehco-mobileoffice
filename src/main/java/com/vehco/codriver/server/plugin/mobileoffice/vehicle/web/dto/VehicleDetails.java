package com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.vehco.codriver.server.plugin.mobileoffice.availability.VehicleAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;
import com.vehco.codriver.server.plugin.mobileoffice.security.LimitedAccess;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.SensorDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph.TachographActivity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Collection;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
public class VehicleDetails implements LimitedAccess {

  private Integer id;
  private String roadBoxId;
  private String internalNr;
  private String licensePlateNr;
  private ConnectedType connectedType;
  private AssetType assetType;
  private VehicleAvailability availability;
  private GpsSnapshot position;
  private TachographActivity lastKnownTachographState;
  private Long lastKnownMileage;
  private Collection<VehicleDetails> connected;
  private Long rootConnectedVehicleId;

  @JsonIgnore
  private Collection<SensorDto> sensors;

  @JsonIgnore
  private Integer customerId;

  @JsonIgnore
  private Collection<Integer> groupIds;

  public VehicleDetails(VehicleDetails old) {
    this.id = old.id;
    this.customerId = old.customerId;
    this.roadBoxId = old.roadBoxId;
    this.internalNr = old.internalNr;
    this.licensePlateNr = old.licensePlateNr;
    this.connectedType = old.connectedType;
    this.assetType = old.assetType;
    this.availability = old.availability;
    this.position = old.position;
    this.lastKnownTachographState = old.lastKnownTachographState;
    this.lastKnownMileage = old.lastKnownMileage;
    this.connected = old.connected;
    this.groupIds = old.groupIds;
    this.sensors = old.sensors;
    this.rootConnectedVehicleId = old.rootConnectedVehicleId;
  }

  public VehicleDetails(
        Integer id,
        Integer customerId,
        String roadBoxId,
        String internalNr,
        String licensePlateNr,
        ConnectedType connectedType,
        AssetType assetType,
        GpsSnapshot position,
        TachographActivity lastKnownTachographState,
        Long lastKnownMileage,
        Long rootConnectedVehicleId,
        Collection<VehicleDetails> connected) {
    this.id = id;
    this.customerId = customerId;
    this.roadBoxId = roadBoxId;
    this.internalNr = internalNr;
    this.licensePlateNr = licensePlateNr;
    this.connectedType = connectedType;
    this.assetType = assetType;
    this.position = position;
    this.lastKnownTachographState = lastKnownTachographState;
    this.lastKnownMileage = lastKnownMileage;
    this.rootConnectedVehicleId = rootConnectedVehicleId;
    this.connected = connected;
  }

  @Override
  public Integer getCustomerId() {
    return customerId;
  }

  @Override
  public Collection<Integer> getGroupIds() {
    return groupIds;
  }

  public void setConnected(Collection<VehicleDetails> connected) {
    this.connected = connected;
  }

  public void setVehicleAvailability(VehicleAvailability availability) {
    this.availability = availability;
  }

  public void setSensors(Collection<SensorDto> sensors) {
    this.sensors = sensors;
  }

  public void setGroupIds(Collection<Integer> groupIds) {
    this.groupIds = groupIds;
  }

  public void resetTrailerFields() {
    this.availability = null;
    this.position = null;
    this.lastKnownTachographState = null;
    this.lastKnownMileage = null;
  }
}
