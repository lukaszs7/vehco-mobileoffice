package com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype;

public enum AssetType {
  VEHICLE,
  MINIBUS,
  CARRIER_CONTAINER,
  CARRIER_TRAILER,
  TEMP_MONITOR,
  TRACTOR
}
