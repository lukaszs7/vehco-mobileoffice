package com.vehco.codriver.server.plugin.mobileoffice.web.transformer;

import com.vehco.codriver.server.common.avro.position.AvroPosition;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.web.dto.GpsPositionResponseDto;
import org.springframework.stereotype.Component;

@Component
public class GpsPositionResponseTransformer {

  public GpsPositionResponseDto transform(AvroPosition position) {
    return GpsPositionResponseDto.builder()
          .latitude(position.getLatitude())
          .longitude(position.getLongitude())
          .direction(position.getHeading())
          .speedKmh(position.getSpeed())
          .timestamp(position.getTimestamp())
          .build();
  }

  public GpsPositionResponseDto transform(GpsSnapshot position) {
    return GpsPositionResponseDto.builder()
          .latitude(position.getLatitude())
          .longitude(position.getLongitude())
          .direction(position.getDirection())
          .speedKmh(position.getSpeedKmh())
          .timestamp(position.getTimestamp())
          .build();
  }
}
