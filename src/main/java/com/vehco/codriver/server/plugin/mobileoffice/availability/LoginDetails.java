package com.vehco.codriver.server.plugin.mobileoffice.availability;

import lombok.Value;

@Value
public class LoginDetails {
  Integer userId;
  Integer vehicleId;
  Long loginTs;
  Long logoutTs;
}
