package com.vehco.codriver.server.plugin.mobileoffice.vehicle;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractDao;
import com.vehco.codriver.server.plugin.mobileoffice.config.JpaVehcoQuery;
import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;
import com.vehco.codriver.server.plugin.mobileoffice.entity.VehicleEntity;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetTypeConverter;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.SensorDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph.TachographStateConverter;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;


@Repository
public class VehicleRepository extends AbstractDao {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AssetTypeConverter assetTypeConverter;
  private final TachographStateConverter tachographStateConverter;
  private final ObjectFactory<JpaVehcoQuery<?>> vehcoQueryDsl;

  public VehicleRepository(
        NamedParameterJdbcTemplate jdbcTemplate,
        AssetTypeConverter assetTypeConverter,
        TachographStateConverter tachographStateConverter,
        ObjectFactory<JpaVehcoQuery<?>> vehcoQueryDsl) {
    super(jdbcTemplate);
    this.assetTypeConverter = assetTypeConverter;
    this.tachographStateConverter = tachographStateConverter;
    this.vehcoQueryDsl = vehcoQueryDsl;
  }

  @Cacheable("vehicles")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Collection<VehicleDetails> getAllActiveVehiclesDetails(Integer customerId) {
    log.debug("Getting all active vehicles for customer {} from database ...", customerId);

    String sql = "" +
          "SELECT\n" +
          "    v.customerid,\n" +
          "    v.vehicleid,\n" +
          "    v.reg_nr AS roadBoxId,\n" +
          "    v.internal_nr AS internalNr,\n" +
          "    v.license_plate_nr AS licensePlateNumber,\n" +
          "    v.assetType,\n" +
          "    pc.timestamp AS 'pc.timestamp',\n" +
          "        pc.la AS 'pc.latitude',\n" +
          "        pc.lo AS 'pc.longitude',\n" +
          "        pc.heading AS 'pc.heading',\n" +
          "        pc.speed AS 'pc.speed',\n" +
          "        pc.[user] AS 'pc.user',\n" +
          "        tet.name AS lastKnownTachoState,\n" +
          "    sse.totalMeters AS lastKnownMileage,\n" +
          "    ct.constantName AS connectedType,\n" +
          "    cv.vehicleid    AS rootConnectedVehicleId\n" +
          "FROM vehicles v\n" +
          "         LEFT JOIN ConnectedVehicles cv ON cv.connectedVehicleid = v.vehicleid\n" +
          "         LEFT JOIN LastKnownTachographState lkts ON lkts.vehicle = v.vehicleid\n" +
          "         LEFT JOIN TachographEventTypes tet ON tet.id = lkts.tachographState\n" +
          "         LEFT JOIN ConnectedType ct ON ct.id = v.connectedType\n" +
          "         RIGHT JOIN PositionCache pc ON pc.vehicle = v.vehicleid\n" +
          "    " +
          "OUTER APPLY (SELECT TOP 1 * FROM StartStopEvents WHERE vehicle = v.vehicleid ORDER BY timestamp DESC) sse\n" +
          "WHERE v.active = 1 AND v.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    return jdbcTemplate.query(sql, params, this::vehicleDetailsMapper);
  }

  @Cacheable("vehicle")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Optional<VehicleDetails> getActiveVehiclesDetails(Integer vehicleId) {
    log.debug("Getting active vehicle {} from database ...", vehicleId);

    String sql = "SELECT v.customerid,\n" +
          "       v.vehicleid,\n" +
          "       v.reg_nr           AS roadBoxId,\n" +
          "       v.internal_nr      AS internalNr,\n" +
          "       v.license_plate_nr AS licensePlateNumber,\n" +
          "       v.assetType,\n" +
          "       pc.timestamp       AS 'pc.timestamp',\n" +
          "       pc.la              AS 'pc.latitude',\n" +
          "       pc.lo              AS 'pc.longitude',\n" +
          "       pc.heading         AS 'pc.heading',\n" +
          "       pc.speed           AS 'pc.speed',\n" +
          "       pc.[user]          AS 'pc.user',\n" +
          "       tet.name           AS lastKnownTachoState,\n" +
          "       sse.totalMeters    AS lastKnownMileage,\n" +
          "       ct.constantName    AS connectedType,\n" +
          "       cv.vehicleid       AS rootConnectedVehicleId\n" +
          "FROM vehicles v\n" +
          "         LEFT JOIN ConnectedVehicles cv ON cv.connectedVehicleid = v.vehicleid\n" +
          "         LEFT JOIN ConnectedType ct ON ct.id = v.connectedType\n" +
          "         LEFT JOIN LastKnownTachographState lkts ON lkts.vehicle = v.vehicleid\n" +
          "         LEFT JOIN TachographEventTypes tet ON tet.id = lkts.tachographState\n" +
          "         LEFT JOIN PositionCache pc ON pc.vehicle = v.vehicleid\n" +
          "         OUTER APPLY (SELECT TOP 1 * FROM StartStopEvents WHERE vehicle = v.vehicleid ORDER BY timestamp DESC) sse\n" +
          "WHERE v.active = 1\n" +
          "  AND v.vehicleid = :vehicleId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("vehicleId", vehicleId);

    VehicleDetails result;

    try {
      result = jdbcTemplate.queryForObject(sql, params, this::vehicleDetailsMapper);
    } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
      result = null;
    }

    return Optional.ofNullable(result);
  }

  @Cacheable("vehicle_sensors")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, Collection<SensorDto>> getAllSensorReadings(Integer customerId) {
    log.debug("Getting latest sensors readings for customer {} from database ...", customerId);

    String sql = "" +
          "SELECT ts.id,\n" +
          "       ts.vehicleId,\n" +
          "       tm.sensorName,\n" +
          "       tm.sensorTemperature,\n" +
          "       tm.measurementTimestamp\n" +
          "FROM TemperatureSensor ts\n" +
          "         CROSS APPLY\n" +
          "     (\n" +
          "         SELECT TOP 1 *\n" +
          "         FROM TemperatureMeasurements\n" +
          "         WHERE ts.sensorAddress = sensorAddress\n" +
          "           AND ts.vehicleId = vehicle\n" +
          "         ORDER BY measurementTimestamp DESC\n" +
          "     ) tm\n" +
          "LEFT JOIN [dbo].vehicles v ON ts.vehicleId = v.vehicleid\n" +
          "WHERE v.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    Map<Integer, Collection<SensorDto>> vehicleSensors = new HashMap<>();

    jdbcTemplate.query(sql, params, row -> {
      Integer vehicleId = row.getInt("vehicleId");
      if (!vehicleSensors.containsKey(vehicleId)) {
        vehicleSensors.put(vehicleId, new ArrayList<>());
      }

      vehicleSensors.get(vehicleId).add(buildSensorDto(row));
    });

    return vehicleSensors;
  }

  @Cacheable("vehicles_groups")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, Collection<Integer>> getVehiclesGroups(Integer customerId) {
    log.debug("Getting vehicles groups for customer {} from database ...", customerId);

    String sql = "" +
          "SELECT vehicle_id, group_id FROM [dbo].vehicle_groups vg\n" +
          "    LEFT JOIN [dbo].vehicles v on v.vehicleid = vg.vehicle_id\n" +
          "WHERE v.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    return executeQueryForVehicleGroups(sql, params);
  }

  @Cacheable("vehicle_groups")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, Collection<Integer>> getVehicleGroups(Integer vehicleId) {
    log.debug("Getting single vehicle {} groups from database ...", vehicleId);

    String sql = "" +
          "SELECT vehicle_id, group_id FROM [dbo].vehicle_groups vg\n" +
          "    LEFT JOIN [dbo].vehicles v on v.vehicleid = vg.vehicle_id\n" +
          "WHERE v.vehicleid = :vehicleId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("vehicleId", vehicleId);

    return executeQueryForVehicleGroups(sql, params);
  }

  @Cacheable("connected-vehicles")
  public List<VehicleEntity> findConnectedVehicles(int vehicleId) {
    return findConnectedVehicles(vehicleId, Collections.emptyList());
  }

  @Cacheable("connected-vehicles")
  public List<VehicleEntity> findConnectedVehicles(int vehicleId, List<ConnectedType> connectedTypes) {
    return vehcoQueryDsl.getObject().fromVehicles()
          .fetchJoinConnectedVehicles()
          .fetchLeftJoinConnectedType()
          .isActiveVehicle()
          .isConnectedVehicleEndTimestampNull()
          .isNotConnectedVehicleConnectedVehicleId(vehicleId)
          .isConnectedVehicleVehicleId(vehicleId)
          .areConnectedTypes(connectedTypes)
          .transformVehicles();
  }

  @Cacheable("connected_vehicles")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, Collection<Integer>> getConnectedVehicles() {
    log.debug("Getting all currently connected and active vehicles");

    String sql = "" +
          "SELECT\n" +
          "       cv.vehicleId,\n" +
          "       cv.connectedVehicleId\n" +
          "FROM ConnectedVehicles cv\n" +
          "LEFT JOIN vehicles v on v.vehicleid = cv.vehicleid\n" +
          "LEFT JOIN vehicles v2 on v2.vehicleid = cv.connectedVehicleid\n" +
          "WHERE cv.endTimestamp IS NULL AND v.active = 1 AND v2.active = 1";

    Map<Integer, Collection<Integer>> connectedVehicles = new HashMap<>();

    jdbcTemplate.query(sql, row -> {
      Integer vehicleId = row.getInt("vehicleId");
      if (!connectedVehicles.containsKey(vehicleId)) {
        connectedVehicles.put(vehicleId, new HashSet<>());
      }

      connectedVehicles.get(vehicleId).add(row.getInt("connectedVehicleId"));
    });

    return connectedVehicles;
  }

  private VehicleDetails vehicleDetailsMapper(ResultSet rs, Integer rowNo) throws SQLException {
    return new VehicleDetails(
          rs.getInt("vehicleId"),
          rs.getInt("customerId"),
          parseNullable(rs.getString("roadBoxId")),
          parseNullable(rs.getString("internalNr")),
          parseNullable(rs.getString("licensePlateNumber")),
          ConnectedType.lookup(rs.getString("connectedType")),
          assetTypeConverter.convert(parseNullable(rs.getString("assetType"))),
          new GpsSnapshot(
                rs.getFloat("pc.latitude"),
                rs.getFloat("pc.longitude"),
                parseNullable(rs.getFloat("pc.speed")),
                parseNullable(rs.getFloat("pc.heading")),
                rs.getLong("pc.timestamp"),
                rs.getInt("pc.user")),
          tachographStateConverter.convert(rs.getString("lastKnownTachoState")),
          rs.getLong("lastKnownMileage"),
          parseNullable(rs.getLong("rootConnectedVehicleId")),
          // temporal
          new ArrayList<>());
  }

  private Map<Integer, Collection<Integer>> executeQueryForVehicleGroups(String sql, MapSqlParameterSource params) {
    Map<Integer, Collection<Integer>> vehicleGroups = new HashMap<>();

    jdbcTemplate.query(sql, params, row -> {
      Integer userId = row.getInt("vehicle_id");
      if (!vehicleGroups.containsKey(userId)) {
        vehicleGroups.put(userId, new ArrayList<>());
      }

      vehicleGroups.get(userId).add(row.getInt("group_id"));
    });

    return vehicleGroups;
  }

  private static SensorDto buildSensorDto(ResultSet row) throws SQLException {
    return new SensorDto(
          row.getInt("id"),
          row.getString("sensorName"),
          row.getString("sensorTemperature"),
          row.getLong("measurementTimestamp"));
  }

}
