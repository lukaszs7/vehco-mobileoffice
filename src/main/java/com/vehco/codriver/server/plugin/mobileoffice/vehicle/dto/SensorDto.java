package com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto;

import lombok.Value;

@Value
public class SensorDto {
  Integer id;
  String name;
  String lastReading;
  Long lastReadingTimestamp;
}
