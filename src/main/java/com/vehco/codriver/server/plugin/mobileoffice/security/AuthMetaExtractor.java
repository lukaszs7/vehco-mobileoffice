package com.vehco.codriver.server.plugin.mobileoffice.security;

import com.vehco.codriver.server.office.WebOfficeSessionContext;
import com.vehco.codriver.server.office.WebOfficeSessionContextHolder;
import com.vehco.codriver.server.office.util.DataAccessRestriction;
import com.vehco.codriver.server.office.util.DataAccessRestrictionUtil;
import com.vehco.codriver.server.services.admin.persist.PersistentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AuthMetaExtractor {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final WebOfficeSessionContextHolder sessionContextHolder;

  public AuthMetaExtractor(WebOfficeSessionContextHolder sessionContextHolder) {
    this.sessionContextHolder = sessionContextHolder;
  }

  public AuthMeta fromSession() {
    WebOfficeSessionContext sessionContext = sessionContextHolder.getCurrentSessionContext();

    if (sessionContext != null) {
      PersistentUser user = sessionContext.getLoggedInUser();

      DataAccessRestriction accessRestriction = DataAccessRestrictionUtil
            .getSessionCachedAccessRestriction(sessionContext, true);

      AuthMeta authMeta = new AuthMeta(
            user.getCustomer().getId(),
            user.getId(),
            accessRestriction.getPermittedGroupIds()
      );

      log.debug("Request auth metadata: {}", authMeta);
      return authMeta;
    }

    throw new IllegalStateException("Unable to extract authentication details from session context");
  }

}
