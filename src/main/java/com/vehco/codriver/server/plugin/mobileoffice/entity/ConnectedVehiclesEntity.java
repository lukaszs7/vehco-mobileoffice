package com.vehco.codriver.server.plugin.mobileoffice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ConnectedVehicles")
public class ConnectedVehiclesEntity {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  @Column(name = "vehicleid")
  private Integer vehicleid;

  @Column(name = "connectedVehicleid")
  private Integer connectedVehicleid;

  @Column(name = "startTimestamp")
  private long startTimestamp;

  @Column(name = "endTimestamp")
  private Long endTimestamp;

  @Column(name = "detectedAutomatically")
  private boolean detectedAutomatically;
}
