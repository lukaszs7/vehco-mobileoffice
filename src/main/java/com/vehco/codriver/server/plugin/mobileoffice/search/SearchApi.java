package com.vehco.codriver.server.plugin.mobileoffice.search;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractApi;
import com.vehco.codriver.server.plugin.mobileoffice.search.dto.SearchItem;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;

import static com.vehco.codriver.server.plugin.mobileoffice.security.Permissions.MOBILE_OFFICE_BASIC;

@RestController
@RequestMapping("webofficeexternalrpc/mobileoffice/search")
public class SearchApi extends AbstractApi {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final SearchService service;

  public SearchApi(AuthMetaExtractor authMetaExtractor, SearchService service) {
    super(authMetaExtractor);
    this.service = service;
  }

  @GetMapping
  @RolesAllowed({MOBILE_OFFICE_BASIC})
  public Collection<SearchItem> search(@RequestParam(name = "q") String query) {
    AuthMeta authMeta = authMetaExtractor.fromSession();
    log.debug("Searching for: {}", query);
    return service.search(query, authMeta);
  }

}
