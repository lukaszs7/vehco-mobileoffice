package com.vehco.codriver.server.plugin.mobileoffice.availability;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserAvailability extends Availability {

  @JsonIgnore
  private Integer vehicleId;

  public UserAvailability(Integer vehicleId, Long loginTs, Long logoutTs) {
    super(loginTs, logoutTs);
    this.vehicleId = vehicleId;
  }

  public Integer getVehicleId() {
    return vehicleId;
  }
}
