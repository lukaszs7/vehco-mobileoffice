package com.vehco.codriver.server.plugin.mobileoffice;

import com.github.benmanes.caffeine.cache.Cache;
import com.vehco.codriver.server.plugin.mobileoffice.live.LivePositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.autoconfigure.health.ConditionalOnEnabledHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@Component("cache")
@ConditionalOnEnabledHealthIndicator("cache")
public class MobileOfficeCacheHealthIndicator implements HealthIndicator {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final CacheManager cacheManager;
  private final LivePositionService livePositionService;

  public MobileOfficeCacheHealthIndicator(
        CacheManager cacheManager,
        LivePositionService livePositionService) {
    this.cacheManager = cacheManager;
    this.livePositionService = livePositionService;
  }

  @Override
  public Health health() {
    Health.Builder status = Health.unknown();

    Map<String, Long> cachedObjects;
    Long liveVehiclePositions;

    try {
      cachedObjects = caffeineCacheObjects();
      liveVehiclePositions = liveVehiclePositionsStored();
    } catch (Exception e) {
      return status.down(e).build();
    }

    // determine status
    if (cachedObjects.isEmpty() && liveVehiclePositions == 0) {
      status.unknown();
    } else {
      status.up();
    }

    // fill details
    Map<String, Long> details = new TreeMap<>();
    details.putAll(cachedObjects);
    details.put("vehicle_current_positions", liveVehiclePositions);

    details.forEach(status::withDetail);
    return status.build();
  }

  private Map<String, Long> caffeineCacheObjects() {
    Map<String, Long> stats = new HashMap<>();

    cacheManager.getCacheNames().forEach(cacheName -> {
      CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
      Cache<Object, Object> nativeCache = cache.getNativeCache();
      Object cachedObject = nativeCache.asMap().values().stream().findFirst().get();
      long elementsCount = -1L;

      if (cachedObject instanceof Collection) {
        elementsCount = ((Collection) cachedObject).size();
      } else if (cachedObject instanceof Map) {
        elementsCount = ((Map) cachedObject).size();
      } else {
        log.error("Unable to estimate cached object size of class: " + cachedObject.getClass());
      }

      stats.put(cacheName, elementsCount);
    });

    return stats;
  }

  private Long liveVehiclePositionsStored() {
    return livePositionService.getCachedVehiclePositions().count();
  }

}
