package com.vehco.codriver.server.plugin.mobileoffice;

import com.vehco.codriver.server.plugin.mobileoffice.live.LivePositionService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractService {

  protected LivePositionService livePositionService;

  @Autowired
  public void setLivePositionService(LivePositionService livePositionService) {
    this.livePositionService = livePositionService;
  }
}
