package com.vehco.codriver.server.plugin.mobileoffice.user;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;


@Repository
public class UserRepository extends AbstractDao {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public UserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
    super(jdbcTemplate);
  }

  @Cacheable("users")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, User> getAllUsers(Integer customerId) {
    log.debug("Getting all users for customer {} from database...", customerId);
    String sql = "" +
          "SELECT\n" +
          "    u.userid,\n" +
          "    coalesce(u.last_name, '') AS last_name,\n" +
          "    coalesce(u.sur_name, '') AS sur_name,\n" +
          "    u.user_name AS user_name,\n" +
          "    u.tel_nr,\n" +
          "    u.email,\n" +
          "    u.customerid\n" +
          "FROM users u\n" +
          "WHERE u.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    Map<Integer, User> users = new HashMap<>();

    jdbcTemplate.query(sql, params, row -> {
      Integer userId = row.getInt("userid");

      User u = new User(
            userId,
            parseNullable(row.getString("last_name")),
            parseNullable(row.getString("sur_name")),
            row.getString("user_name"),
            parseNullable(row.getString("tel_nr")),
            parseNullable(row.getString("email")),
            row.getInt("customerid"));

      users.put(userId, u);
    });

    return users;
  }

  @Cacheable("users_groups")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Map<Integer, Collection<Integer>> getUserGroups(Integer customerId) {
    log.info("Getting all user groups for customer {} from database...", customerId);

    String sql = "" +
          "SELECT\n" +
          "    user_id,\n" +
          "    group_id\n" +
          "FROM user_groups\n" +
          "    LEFT JOIN users u on u.userid = user_groups.user_id\n" +
          "WHERE u.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    Map<Integer, Collection<Integer>> userGroups = new HashMap<>();

    jdbcTemplate.query(sql, params, row -> {
      Integer userId = row.getInt("user_id");
      if (!userGroups.containsKey(userId)) {
        userGroups.put(userId, new ArrayList<>());
      }

      userGroups.get(userId).add(row.getInt("group_id"));
    });

    return userGroups;
  }

}
