package com.vehco.codriver.server.plugin.mobileoffice.config;

import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {
  public static final String HIBERNATE_DEFAULT_PROPERTIES_BEAN = "hibernateProperties";
  public static final String HIBERNATE_ENHANCED_PROPERTIES_BEAN = "enhancedHibernateProperties";
  public static final List<String> ENTITIES_TO_SCAN = List.of(
        "com.vehco.codriver.server.plugin.mobileoffice.entity"
  );

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                     @Qualifier(HIBERNATE_ENHANCED_PROPERTIES_BEAN)
                                                                           Properties jpaProperties) {
    final LocalContainerEntityManagerFactoryBean em
          = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource);
    em.setPackagesToScan(ENTITIES_TO_SCAN.toJavaArray(String[]::new));

    final JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    em.setJpaProperties(jpaProperties);
    return em;
  }

  @Bean(HIBERNATE_ENHANCED_PROPERTIES_BEAN)
  @Profile(ProfilesConfig.DEBUG_PROFILE)
  public Properties hibernateDebugProperties(@Qualifier(HIBERNATE_DEFAULT_PROPERTIES_BEAN) Properties jpaProperties) {
    jpaProperties.put("hibernate.show_sql", "true");
    jpaProperties.put("hibernate.format_sql", "true");
    return jpaProperties;
  }

  @Bean(HIBERNATE_ENHANCED_PROPERTIES_BEAN)
  @Profile("!" + ProfilesConfig.DEBUG_PROFILE)
  public Properties hibernateNonDebugProperties(
        @Qualifier(HIBERNATE_DEFAULT_PROPERTIES_BEAN) Properties jpaProperties) {
    return jpaProperties;
  }

  @Bean
  public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
    return entityManagerFactory.createEntityManager();
  }
}
