package com.vehco.codriver.server.plugin.mobileoffice.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.vehco.codriver.server.plugin.mobileoffice.availability.VehicleAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph.TachographActivity;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
@Value
public class VehicleResponseDto {
  long id;
  String internalNr;
  String licensePlateNr;
  ConnectedType connectedType;
  AssetType assetType;
  Option<VehicleAvailability> availability;
  Option<TachographActivity> lastKnownTachographState;
  List<VehicleResponseDto> connected;

  @JsonCreator
  public VehicleResponseDto(long id, String internalNr, String licensePlateNr, ConnectedType connectedType,
                            AssetType assetType,
                            Option<VehicleAvailability> availability,
                            Option<TachographActivity> lastKnownTachographState, List<VehicleResponseDto> connected) {
    this.id = id;
    this.internalNr = internalNr;
    this.licensePlateNr = licensePlateNr;
    this.connectedType = connectedType;
    this.assetType = assetType;
    this.availability = availability;
    this.lastKnownTachographState = lastKnownTachographState;
    this.connected = connected;
  }
}
