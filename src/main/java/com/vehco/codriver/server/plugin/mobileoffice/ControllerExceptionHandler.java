package com.vehco.codriver.server.plugin.mobileoffice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(value = NoSuchElementException.class)
  public ResponseEntity<ApiErrorMessage> resourceNotFound(Exception e, WebRequest req) {
    ApiErrorMessage msg = new ApiErrorMessage("Requested entity not found", e);
    return new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
  }

}
