package com.vehco.codriver.server.plugin.mobileoffice.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
@EqualsAndHashCode
public class LivePositionResponseDto {
  Option<VehicleResponseDto> vehicle;
  Option<UserResponseDto> driver;
  GpsPositionResponseDto position;

  @JsonCreator
  public LivePositionResponseDto(Option<VehicleResponseDto> vehicle, Option<UserResponseDto> driver,
                                 GpsPositionResponseDto position) {
    this.vehicle = vehicle;
    this.driver = driver;
    this.position = position;
  }
}
