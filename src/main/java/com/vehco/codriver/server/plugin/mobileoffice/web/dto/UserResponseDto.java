package com.vehco.codriver.server.plugin.mobileoffice.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
@EqualsAndHashCode
public class UserResponseDto {
  long id;
  Option<String> lastName;
  Option<String> surName;
  String userName;
  Option<String> email;

  @JsonCreator
  public UserResponseDto(long id, Option<String> lastName, Option<String> surName, String userName,
                         Option<String> email) {
    this.id = id;
    this.lastName = lastName;
    this.surName = surName;
    this.userName = userName;
    this.email = email;
  }
}
