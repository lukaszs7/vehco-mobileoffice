package com.vehco.codriver.server.plugin.mobileoffice.entity.converters;

import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;

import javax.persistence.AttributeConverter;

public class ConnectedTypeConverter implements AttributeConverter<ConnectedType, String> {
  @Override
  public String convertToDatabaseColumn(ConnectedType attribute) {
    return attribute.getDatabaseValue();
  }

  @Override
  public ConnectedType convertToEntityAttribute(String dbData) {
    return ConnectedType.lookup(dbData);
  }
}
