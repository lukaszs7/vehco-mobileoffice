package com.vehco.codriver.server.plugin.mobileoffice.live;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractApi;
import com.vehco.codriver.server.plugin.mobileoffice.live.dto.LiveElementDto;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;

import static com.vehco.codriver.server.plugin.mobileoffice.security.Permissions.MOBILE_OFFICE_BASIC;

@RestController
@RequestMapping("webofficeexternalrpc/mobileoffice/live")
public class LiveVehicleApi extends AbstractApi {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final LivePositionService service;

  public LiveVehicleApi(AuthMetaExtractor authMetaExtractor, LivePositionService service) {
    super(authMetaExtractor);
    this.service = service;
  }

  @GetMapping("/snapshot")
  @RolesAllowed({MOBILE_OFFICE_BASIC})
  public Collection<LiveElementDto> snapshot() {
    AuthMeta authMeta = authMetaExtractor.fromSession();
    log.debug("Getting live vehicle positions snapshot");
    return service.getSnapshot(authMeta);
  }

  @GetMapping("/events")
  @RolesAllowed({MOBILE_OFFICE_BASIC})
  public SseEmitter events() {
    AuthMeta authMeta = authMetaExtractor.fromSession();
    log.debug("Start emitting live vehicle positions events");
    return service.triggerSseEmitter(authMeta);
  }

}
