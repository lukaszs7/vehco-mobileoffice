package com.vehco.codriver.server.plugin.mobileoffice;

import lombok.Getter;

@Getter
public class ApiErrorMessage {
  private long timestamp;
  private String description;
  private String stackTraceMessage;

  public ApiErrorMessage(String description, Exception ex) {
    this.timestamp = System.currentTimeMillis();
    this.description = description;
    this.stackTraceMessage = ex.getLocalizedMessage();
  }
}
