package com.vehco.codriver.server.plugin.mobileoffice.user;

import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import lombok.Value;

@Value
public class DriverCurrentVehicleDto {
  Integer vehicleId;
  String roadBoxId;
  String internalNr;
  String licensePlateNr;
  AssetType assetType;
  GpsSnapshot position;
}
