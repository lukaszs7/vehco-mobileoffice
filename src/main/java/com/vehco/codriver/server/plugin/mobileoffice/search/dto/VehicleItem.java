package com.vehco.codriver.server.plugin.mobileoffice.search.dto;

import com.vehco.codriver.server.plugin.mobileoffice.availability.LoginStatus;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;

public class VehicleItem extends SearchItem {

  private static final String ITEM_TYPE = "VEHICLE";

  private String roadBoxId;
  private String internalNr;
  private String licensePlateNr;
  private AssetType assetType;

  public VehicleItem(
        Integer id,
        LoginStatus loginStatus,
        GpsSnapshot position,
        String roadBoxId,
        String internalNr,
        String licensePlateNr,
        AssetType assetType) {
    super(id, loginStatus, position);
    this.roadBoxId = roadBoxId;
    this.internalNr = internalNr;
    this.licensePlateNr = licensePlateNr;
    this.assetType = assetType;
  }

  public String getRoadBoxId() {
    return roadBoxId;
  }

  public String getInternalNr() {
    return internalNr;
  }

  public String getLicensePlateNr() {
    return licensePlateNr;
  }

  public AssetType getAssetType() {
    return assetType;
  }

  @Override
  public String getType() {
    return ITEM_TYPE;
  }
}
