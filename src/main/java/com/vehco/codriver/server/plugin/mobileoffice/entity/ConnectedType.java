package com.vehco.codriver.server.plugin.mobileoffice.entity;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum ConnectedType {
  ROOT("root"), SELECTABLE("selectable"), FIXED("fixed");
  private String databaseValue;

  ConnectedType(String databaseValue) {
    this.databaseValue = databaseValue;
  }

  public static ConnectedType lookup(String databaseValue) {
    return Arrays.stream(ConnectedType.values())
          .filter(connectedType -> connectedType.getDatabaseValue().equals(databaseValue))
          .findFirst()
          .orElseThrow(() -> new IllegalArgumentException(String.format("Cannot find %s attribute in the %s enum",
                databaseValue, ConnectedType.class.getName())));
  }
}
