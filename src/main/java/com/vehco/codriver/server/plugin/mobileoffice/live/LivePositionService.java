package com.vehco.codriver.server.plugin.mobileoffice.live;

import com.vehco.codriver.server.common.avro.position.AvroPosition;
import com.vehco.codriver.server.plugin.mobileoffice.live.dto.CurrentVehiclePosition;
import com.vehco.codriver.server.plugin.mobileoffice.live.dto.LiveElementDto;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.HasAccessPredicate;
import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.user.UserService;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.VehicleService;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.VehicleDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import com.vehco.codriver.server.plugin.mobileoffice.web.dto.LivePositionResponseDto;
import com.vehco.codriver.server.plugin.mobileoffice.web.transformer.LiveDtoResponseTransformer;
import com.vehco.codriver.server.plugin.mobileoffice.web.transformer.VehicleDtoResponseTransformer;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class LivePositionService {

  private static final Integer QUEUE_SIZE = 100;

  private final Logger log = LoggerFactory.getLogger(getClass());

  // Store latest vehicle positions
  private final Map<Integer, CurrentVehiclePosition> currentPositions = new ConcurrentHashMap<>();

  // Store latest elements from Kafka topic (used for polling from another threads by SSE)
  private final Queue<CurrentVehiclePosition> tmpPositionsQueue = new LinkedBlockingQueue<>(QUEUE_SIZE);

  private final HasAccessPredicate hasAccessPredicate;
  private final AvroPositionConverter positionConverter;
  private final TaskExecutor sseTaskExecutor;
  private final VehicleDtoResponseTransformer vehicleDtoResponseTransformer;
  private final LiveDtoResponseTransformer liveDtoResponseTransformer;

  private VehicleService vehicleService;
  private UserService userService;

  @Autowired
  public void setVehicleService(VehicleService vehicleService) {
    this.vehicleService = vehicleService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @KafkaListener(topics = "${mobileoffice.kafka.topics.vehicle-positions-name}")
  public void receive(
        @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Integer vehicleId,
        @Payload AvroPosition payload) {

    log.trace("received payload: {}, {}", vehicleId, payload);
    CurrentVehiclePosition position = positionConverter.convert(payload);

    // Process only active vehicles
    vehicleService.findOneVehicleDetails(vehicleId)
          // filter positions / vehicles already connected to root vehicle
          .filter(vd -> vd.getRootConnectedVehicleId() == null)
          .ifPresent(vd -> {
            augmentPositionWithExtraData(position, vd);
            storeCurrentPositionToQueue(position);
            storeCurrentPositionToMap(position);
          });
  }

  public Map<Integer, CurrentVehiclePosition> getCachedCurrentPositions() {
    return currentPositions;
  }

  public Stream<CurrentVehiclePosition> getCachedVehiclePositions() {
    return currentPositions.values().stream();
  }

  public Queue<CurrentVehiclePosition> getCachedVehiclePositionsQueue() {
    return tmpPositionsQueue;
  }

  public GpsSnapshot getVehicleGpsPosition(Integer vehicleId) {
    // First try latest position from cache, fallback to db if not found
    return getLivePosition(vehicleId)
          .orElse(vehicleService.findOneVehicleDetails(vehicleId).map(VehicleDetails::getPosition).orElse(null));
  }

  public Collection<LiveElementDto> getSnapshot(AuthMeta authMeta) {
    log.debug("Start sending filtered stored positions, auth: " + authMeta);

    // Find all vehicles for current user using DB
    Map<Integer, VehicleDetails> all = vehicleService.getAllVehicleDetails(authMeta);

    // separate trucks and trailers that are connected
    Map<Integer, Collection<Integer>> connected = vehicleService.findConnectedVehicles();
    Collection<Integer> connectedTruckIds = connected.keySet();
    Collection<Integer> connectedTrailerIds = connected.values().stream().flatMap(Collection::stream).collect(toList());

    // Assign trailers to trucks
    all.entrySet().stream()
          .filter(e -> connectedTruckIds.contains(e.getKey()))
          .forEach(e -> {
            VehicleDetails truck = e.getValue();

            List<VehicleDetails> trailers = all.values().stream()
                  .filter(trailer -> connected.get(truck.getId()).contains(trailer.getId()))
                  .peek(VehicleDetails::resetTrailerFields)
                  .collect(toList());

            truck.setConnected(trailers);
          });

    // Remove all connected trailers from the response
    all.entrySet().removeIf(e -> connectedTrailerIds.contains(e.getKey()));

    // Build final response
    return all.values().stream()
          .map(vd -> {
            Integer vehicleId = vd.getId();
            GpsSnapshot gps = getLivePosition(vehicleId).orElse(vd.getPosition());
            return buildLiveElementDtoObject(gps, vd, gps.getDriverId(), authMeta);
          })
          .collect(toList());
  }

  public SseEmitter triggerSseEmitter(AuthMeta authMeta) {
    SseEmitter sseEmitter = new SseEmitter(Long.MAX_VALUE);

    SseEmitterThread task = new SseEmitterThread(
          authMeta,
          sseEmitter,
          this,
          hasAccessPredicate);

    sseTaskExecutor.execute(task);

    return sseEmitter;
  }

  public LivePositionResponseDto buildLiveElementDtoObject(
        GpsSnapshot pos, Integer vehicleId, Integer userId, AuthMeta authMeta) {
    // TODO it may be done in single query
    final Optional<VehicleDetails> vehicle = vehicleService.findOneVehicleDetails(vehicleId).map(vd -> {
      // Connected truck details
      final List<VehicleDto> connectedVehicles = vehicleService.findConnectedVehiclesForVehicle(vehicleId);
      vd.setConnected(vehicleDtoResponseTransformer.transformCollection(connectedVehicles));
      return vd;
    });

    final Option<User> userDetails = Option.ofOptional(userService.findOneUser(userId, authMeta));
    final Option<VehicleDetails> optionVehicle = Option.ofOptional(vehicle);

    return liveDtoResponseTransformer.transform(pos, optionVehicle, userDetails);
  }

  private LiveElementDto buildLiveElementDtoObject(
        GpsSnapshot pos, VehicleDetails vd, Integer userId, AuthMeta authMeta) {
    return new LiveElementDto(vd, userService.findOneUser(userId, authMeta).orElse(null), pos);
  }

  private Optional<GpsSnapshot> getLivePosition(Integer vehicleId) {
    return getCachedVehiclePositions()
          .filter(p -> p.getVehicleId().equals(vehicleId))
          .map(pos -> new GpsSnapshot(
                pos.getLatitude(),
                pos.getLongitude(),
                pos.getSpeed(),
                pos.getHeading(),
                pos.getTimestamp(),
                pos.getUserId()))
          .findFirst();
  }

  private void augmentPositionWithExtraData(CurrentVehiclePosition cvp, VehicleDetails vd) {
    Integer vehicleId = cvp.getVehicleId();

    cvp.setCustomerId(vd.getCustomerId());
    cvp.setGroupIds(vehicleService.findVehicleGroups(vehicleId));
  }

  private void storeCurrentPositionToQueue(CurrentVehiclePosition cvp) {
    // evict head element from queue if full
    if (tmpPositionsQueue.size() >= QUEUE_SIZE) {
      tmpPositionsQueue.poll();
    }

    tmpPositionsQueue.offer(cvp);
  }

  private void storeCurrentPositionToMap(CurrentVehiclePosition cvp) {
    currentPositions.put(cvp.getVehicleId(), cvp);
  }
}
