package com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto;

import com.vehco.codriver.server.plugin.mobileoffice.availability.Availability;
import com.vehco.codriver.server.plugin.mobileoffice.entity.ConnectedType;
import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetType;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph.TachographActivity;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Value
public class VehicleDto {
  Integer id;
  String roadBoxId;
  Option<String> internalNr;
  String licensePlateNr;
  ConnectedType connectedType;
  AssetType assetType;
  Option<Availability> availability;
  Collection<SensorDto> sensors;
  List<VehicleDto> connectedVehicles;
  Option<GpsSnapshot> position;
  Option<Long> lastKnownMileage;
  Option<TachographActivity> lastKnownTachographState;
  Option<User> driver;
}
