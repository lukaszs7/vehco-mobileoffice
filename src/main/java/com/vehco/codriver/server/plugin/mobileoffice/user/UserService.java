package com.vehco.codriver.server.plugin.mobileoffice.user;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractService;
import com.vehco.codriver.server.plugin.mobileoffice.availability.AvailabilityService;
import com.vehco.codriver.server.plugin.mobileoffice.availability.UserAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.HasAccessPredicate;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

@Service
@RequiredArgsConstructor
public class UserService extends AbstractService {

  private final HasAccessPredicate hasAccessPredicate;
  private final AvailabilityService availabilityService;
  private final UserRepository userRepository;

  private VehicleService vehicleService;

  // setter injection due to circular dependency (UserService <-> VehicleService)
  @Autowired
  public void setVehicleService(VehicleService vehicleService) {
    this.vehicleService = vehicleService;
  }

  public Optional<DriverDto> findDriver(Integer userId, AuthMeta authMeta) {
    return findOneUser(userId, authMeta)
          .filter(u -> hasAccessPredicate.test(u, authMeta))
          .map(u -> new DriverDto(
                u.getId(),
                u.getLastName(),
                u.getSurName(),
                u.getUserName(),
                u.getPhoneNumber(),
                u.getEmail(),
                u.getAvailability(),
                Optional.ofNullable(u.getAvailability())
                      .map(UserAvailability::getVehicleId)
                      .flatMap(v -> getCurrentVehicleDetails(v, authMeta))
                      .orElse(null)));
  }

  public Map<Integer, Collection<Integer>> findUsersGroups(Integer customerId) {
    return userRepository.getUserGroups(customerId);
  }

  public Optional<User> findOneUser(Integer userId, AuthMeta authMeta) {
    Map<Integer, User> users = userRepository.getAllUsers(authMeta.getCustomerId());
    return Optional.ofNullable(users.get(userId))
          .map(u -> enhanceWithAvailability(u, authMeta))
          .map(this::enhanceWithGroups);
  }

  public Map<Integer, User> findAllUsers(AuthMeta authMeta) {
    return userRepository.getAllUsers(authMeta.getCustomerId()).entrySet().stream()
          .collect(toMap(Map.Entry::getKey, e ->
                Optional.of(e.getValue())
                      .map(u -> enhanceWithAvailability(u, authMeta))
                      .map(this::enhanceWithGroups)
                      .get()
          ));
  }

  private Optional<DriverCurrentVehicleDto> getCurrentVehicleDetails(Integer vehicleId, AuthMeta authMeta) {
    return vehicleService.findOneVehicleDetails(vehicleId, authMeta)
          .map(vd -> new DriverCurrentVehicleDto(
                vd.getId(),
                vd.getRoadBoxId(),
                vd.getInternalNr(),
                vd.getLicensePlateNr(),
                vd.getAssetType(),
                livePositionService.getVehicleGpsPosition(vehicleId)));
  }

  private User enhanceWithAvailability(User old, AuthMeta authMeta) {
    User u = new User(old);
    availabilityService.getUserAvailability(old.getId(), authMeta.getCustomerId()).ifPresent(u::setAvailability);
    return u;
  }

  private User enhanceWithGroups(User old) {
    User u = new User(old);
    u.setGroupIds(findUsersGroups(old.getCustomerId()).getOrDefault(u.getId(), new ArrayList<>()));
    return u;
  }

}
