package com.vehco.codriver.server.plugin.mobileoffice.vehicle;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractService;
import com.vehco.codriver.server.plugin.mobileoffice.availability.AvailabilityService;
import com.vehco.codriver.server.plugin.mobileoffice.availability.LoginStatus;
import com.vehco.codriver.server.plugin.mobileoffice.availability.VehicleAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.entity.VehicleEntity;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.HasAccessPredicate;
import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.user.UserService;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.SensorDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.VehicleDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.transformer.VehicleEntityToDtoTransformer;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Service
@RequiredArgsConstructor
public class VehicleService extends AbstractService {

  private final VehicleRepository vehicleRepository;
  private final AvailabilityService availabilityService;
  private final HasAccessPredicate hasAccessPredicate;
  private final VehicleEntityToDtoTransformer vehicleEntityToDtoTransformer;

  private UserService userService;

  // setter injection due to circular dependency (UserService <-> VehicleService)
  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public List<VehicleDto> findConnectedVehiclesForVehicle(int vehicleId) {
    final List<VehicleEntity> connectedVehicles = vehicleRepository.findConnectedVehicles(vehicleId);
    return vehicleEntityToDtoTransformer.transformCollection(connectedVehicles);
  }

  public Optional<VehicleDto> findOne(Integer vehicleId, AuthMeta authMeta) {
    return findOneVehicleDetails(vehicleId, authMeta)
          .filter(vd -> hasAccessPredicate.test(vd, authMeta))
          .map(vd -> new VehicleDto(
                vd.getId(),
                vd.getRoadBoxId(),
                Option.of(vd.getInternalNr()),
                vd.getLicensePlateNr(),
                // TODO fill connected type
                null,
                vd.getAssetType(),
                Option.of(vd.getAvailability()),
                vd.getSensors(),
                Collections.emptyList(),
                Option.of(livePositionService.getVehicleGpsPosition(vd.getId())),
                Option.of(vd.getLastKnownMileage()),
                Option.of(vd.getLastKnownTachographState()),
                Option.of(vd.getAvailability())
                      .map(VehicleAvailability::getDriverId)
                      .flatMap(did -> Option.ofOptional(getDriver(vd.getAvailability().getStatus(), did, authMeta)))
          ));
  }

  public Optional<VehicleDetails> findOneVehicleDetails(Integer vehicleId) {
    return vehicleRepository.getActiveVehiclesDetails(vehicleId);
  }

  public Optional<VehicleDetails> findOneVehicleDetails(Integer vehicleId, AuthMeta authMeta) {
    Integer customerId = authMeta.getCustomerId();
    return vehicleRepository.getAllActiveVehiclesDetails(customerId).stream()
          .filter(v -> v.getId().equals(vehicleId))
          .map(v -> enhanceWithAvailability(v, customerId))
          .map(v -> enhanceWithGroups(v, customerId))
          .map(v -> enhanceWithSensors(v, customerId))
          .findAny();
  }

  public Map<Integer, VehicleDetails> getAllVehicleDetails(AuthMeta authMeta) {
    Integer customerId = authMeta.getCustomerId();
    return vehicleRepository.getAllActiveVehiclesDetails(customerId).stream()
          .map(v -> enhanceWithAvailability(v, customerId))
          .map(v -> enhanceWithGroups(v, customerId))
          .map(v -> enhanceWithSensors(v, customerId))
          .collect(toMap(
                VehicleDetails::getId,
                Function.identity()));
  }

  public Collection<Integer> findVehicleGroups(Integer vehicleId) {
    return vehicleRepository.getVehicleGroups(vehicleId).getOrDefault(vehicleId, new ArrayList<>());
  }

  private Collection<Integer> findVehicleGroups(Integer vehicleId, Integer customerId) {
    return vehicleRepository.getVehiclesGroups(customerId).getOrDefault(vehicleId, new ArrayList<>());
  }

  public Map<Integer, Collection<Integer>> findConnectedVehicles() {
    return vehicleRepository.getConnectedVehicles();
  }

  private Collection<SensorDto> findVehicleSensors(Integer vehicleId, Integer customerId) {
    return findVehiclesSensors(customerId).getOrDefault(vehicleId, new ArrayList<>());
  }

  private VehicleDetails enhanceWithAvailability(VehicleDetails old, Integer customerId) {
    VehicleDetails vd = new VehicleDetails(old);
    availabilityService.getVehicleAvailability(old.getId(), customerId).ifPresent(vd::setVehicleAvailability);
    return vd;
  }

  private VehicleDetails enhanceWithGroups(VehicleDetails old, Integer customerId) {
    VehicleDetails vd = new VehicleDetails(old);
    vd.setGroupIds(findVehicleGroups(vd.getId(), customerId));
    return vd;
  }

  private VehicleDetails enhanceWithSensors(VehicleDetails old, Integer customerId) {
    VehicleDetails vd = new VehicleDetails(old);
    vd.setSensors(findVehicleSensors(vd.getId(), customerId));
    return vd;
  }

  private Map<Integer, Collection<SensorDto>> findVehiclesSensors(Integer customerId) {
    return vehicleRepository.getAllSensorReadings(customerId);
  }

  private Optional<User> getDriver(LoginStatus vehicleLoginStatus, Integer userId, AuthMeta authMeta) {
    if (vehicleLoginStatus.equals(LoginStatus.LOGGED_IN)) {
      return userService.findOneUser(userId, authMeta);
    } else {
      return Optional.empty();
    }
  }

}
