package com.vehco.codriver.server.plugin.mobileoffice.entity;

import com.vehco.codriver.server.plugin.mobileoffice.entity.converters.ConnectedTypeConverter;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "ConnectedType")
public class ConnectedTypeEntity {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  @Convert(converter = ConnectedTypeConverter.class)
  @Column(name = "constantName")
  private ConnectedType connectedType;
}
