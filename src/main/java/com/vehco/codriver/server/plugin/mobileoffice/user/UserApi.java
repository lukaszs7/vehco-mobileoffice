package com.vehco.codriver.server.plugin.mobileoffice.user;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractApi;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

import static com.vehco.codriver.server.plugin.mobileoffice.security.Permissions.MOBILE_OFFICE_BASIC;

@RestController
@RequestMapping("webofficeexternalrpc/mobileoffice/user")
public class UserApi extends AbstractApi {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final UserService service;

  public UserApi(AuthMetaExtractor authMetaExtractor, UserService service) {
    super(authMetaExtractor);
    this.service = service;
  }

  @GetMapping("/{id}")
  @RolesAllowed({MOBILE_OFFICE_BASIC})
  public DriverDto findOne(@PathVariable Integer id) {
    AuthMeta authMeta = authMetaExtractor.fromSession();
    log.debug("Getting user {} details", id);
    return service.findDriver(id, authMeta).get();
  }

}
