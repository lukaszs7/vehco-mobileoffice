package com.vehco.codriver.server.plugin.mobileoffice.user;


import com.vehco.codriver.server.plugin.mobileoffice.availability.Availability;
import lombok.Value;

@Value
public class DriverDto {
  Integer id;
  String lastName;
  String surName;
  String userName;
  String phoneNumber;
  String email;
  Availability availability;
  DriverCurrentVehicleDto vehicle;
}
