package com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph;

import com.vehco.codriver.foundation.modules.v2tachograph.record.type.AvailableActivatedRecord;
import com.vehco.codriver.foundation.modules.v2tachograph.record.type.DrivingActivatedRecord;
import com.vehco.codriver.foundation.modules.v2tachograph.record.type.RestingActivatedRecord;
import com.vehco.codriver.foundation.modules.v2tachograph.record.type.WorkingActivatedRecord;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TachographStateConverter implements Converter<String, TachographActivity> {

  private static final String TACHO_STATE_DRIVING = DrivingActivatedRecord.class.getName();
  private static final String TACHO_STATE_RESTING = RestingActivatedRecord.class.getName();
  private static final String TACHO_STATE_WORKING = WorkingActivatedRecord.class.getName();
  private static final String TACHO_STATE_AVAILABLE = AvailableActivatedRecord.class.getName();

  @Override
  public TachographActivity convert(String activityType) {
    if (TACHO_STATE_DRIVING.equals(activityType)) {
      return TachographActivity.DRIVING;
    } else if (TACHO_STATE_RESTING.equals(activityType)) {
      return TachographActivity.RESTING;
    } else if (TACHO_STATE_WORKING.equals(activityType)) {
      return TachographActivity.WORKING;
    } else if (TACHO_STATE_AVAILABLE.equals(activityType)) {
      return TachographActivity.AVAILABLE;
    } else {
      return TachographActivity.UNKNOWN;
    }
  }
}
