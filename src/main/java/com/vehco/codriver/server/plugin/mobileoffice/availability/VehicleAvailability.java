package com.vehco.codriver.server.plugin.mobileoffice.availability;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class VehicleAvailability extends Availability {

  @JsonIgnore
  private Integer driverId;

  public VehicleAvailability(Integer driverId, Long loginTs, Long logoutTs) {
    super(loginTs, logoutTs);
    this.driverId = driverId;
  }

  public Integer getDriverId() {
    return driverId;
  }
}
