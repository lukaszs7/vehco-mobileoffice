package com.vehco.codriver.server.plugin.mobileoffice.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
@EqualsAndHashCode
public class GpsPositionResponseDto {
  Float latitude;
  Float longitude;
  Float speedKmh;
  Float direction;
  Long timestamp;

  @JsonCreator
  public GpsPositionResponseDto(Float latitude, Float longitude, Float speedKmh, Float direction, Long timestamp) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.speedKmh = speedKmh;
    this.direction = direction;
    this.timestamp = timestamp;
  }
}
