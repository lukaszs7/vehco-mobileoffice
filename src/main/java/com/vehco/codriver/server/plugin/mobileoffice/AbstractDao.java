package com.vehco.codriver.server.plugin.mobileoffice;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class AbstractDao {

  protected final NamedParameterJdbcTemplate jdbcTemplate;

  protected AbstractDao(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  protected static <T> T parseNullable(Object value) {
    if (value == null) {
      return null;
    }

    if (value instanceof String && ((String) value).length() == 0) {
      return null;
    }

    if (value instanceof Integer && ((Integer) value) == 0) {
      return null;
    }

    if (value instanceof Long && ((Long) value) == 0) {
      return null;
    }

    return (T) value;
  }

}
