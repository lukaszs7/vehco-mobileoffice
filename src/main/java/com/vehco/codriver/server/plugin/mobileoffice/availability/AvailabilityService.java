package com.vehco.codriver.server.plugin.mobileoffice.availability;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AvailabilityService {

  private final AvailabilityRepository repository;

  public Optional<VehicleAvailability> getVehicleAvailability(Integer vehicleId, Integer customerId) {
    /*
     * Vehicles:
     * 1) If not ATX: Use login_cache (done)
     * 2) If ATX: Use last known position time. If > 10 minutes then logged out.
     */
    return repository.getAllLoginCaches(customerId).stream()
          .filter(lc -> lc.getVehicleId().equals(vehicleId))
          .max(Comparator.comparing(LoginDetails::getLoginTs))
          .map(lc -> new VehicleAvailability(lc.getUserId(), lc.getLoginTs(), lc.getLogoutTs()));
  }

  public Optional<UserAvailability> getUserAvailability(Integer userId, Integer customerId) {
    /*
     * Drivers:
     * 1) login_cache (done)
     * 2) Last start/stop event
     * 3) Tachograph card inserted in vehicle --> map to user
     */
    return repository.getAllLoginCaches(customerId).stream()
          .filter(lc -> lc.getUserId().equals(userId))
          .max(Comparator.comparing(LoginDetails::getLoginTs))
          .map(lc -> new UserAvailability(lc.getVehicleId(), lc.getLoginTs(), lc.getLogoutTs()));
  }

}
