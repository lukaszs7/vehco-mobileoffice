package com.vehco.codriver.server.plugin.mobileoffice.search.dto;

import com.vehco.codriver.server.plugin.mobileoffice.availability.LoginStatus;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;

public class DriverItem extends SearchItem {

  private static final String ITEM_TYPE = "DRIVER";

  private String surName;
  private String lastName;
  private String userName;

  public DriverItem(
        Integer id,
        LoginStatus loginStatus,
        GpsSnapshot position,
        String surName,
        String lastName,
        String userName) {
    super(id, loginStatus, position);
    this.surName = surName;
    this.lastName = lastName;
    this.userName = userName;
  }

  public String getSurName() {
    return surName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getUserName() {
    return userName;
  }

  @Override
  public String getType() {
    return ITEM_TYPE;
  }
}
