package com.vehco.codriver.server.plugin.mobileoffice.availability;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@Repository
public class AvailabilityRepository extends AbstractDao {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public AvailabilityRepository(NamedParameterJdbcTemplate jdbcTemplate) {
    super(jdbcTemplate);
  }

  @Cacheable("login_cache")
  @SuppressWarnings({
        "checkstyle:OperatorWrap",
        "checkstyle:LineLength"})
  @Transactional(propagation = SUPPORTS)
  public Collection<LoginDetails> getAllLoginCaches(Integer customerId) {
    log.debug("Getting login cache details for customer {} from database...", customerId);

    String sql = "" +
          "SELECT * FROM login_cache lc\n" +
          "    LEFT JOIN users u ON lc.userid = u.userid\n" +
          "WHERE u.customerid = :customerId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("customerId", customerId);

    return jdbcTemplate.query(sql, params, (rs,rowNum) -> new LoginDetails(
          rs.getInt("userid"),
          rs.getInt("vehicleid"),
          parseNullable(rs.getLong("login")),
          parseNullable(rs.getLong("logout"))));
  }

}
