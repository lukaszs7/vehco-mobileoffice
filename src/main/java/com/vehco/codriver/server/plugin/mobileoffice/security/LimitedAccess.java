package com.vehco.codriver.server.plugin.mobileoffice.security;

import java.util.Collection;

public interface LimitedAccess {

  Integer getCustomerId();

  Collection<Integer> getGroupIds();

}
