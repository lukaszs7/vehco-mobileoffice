package com.vehco.codriver.server.plugin.mobileoffice.availability;

public enum LoginStatus {
  NEVER_LOGGED_IN,
  LOGGED_IN,
  LOGGED_OUT,
  UNKNOWN
}
