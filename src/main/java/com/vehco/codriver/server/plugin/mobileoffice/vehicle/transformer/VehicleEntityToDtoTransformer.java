package com.vehco.codriver.server.plugin.mobileoffice.vehicle.transformer;

import com.vehco.codriver.server.plugin.mobileoffice.entity.VehicleEntity;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.assettype.AssetTypeConverter;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.VehicleDto;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class VehicleEntityToDtoTransformer {

  private final AssetTypeConverter assetTypeConverter;

  public VehicleDto transform(VehicleEntity vehicleEntity) {
    return new VehicleDto(
          vehicleEntity.getVehicleid(),
          vehicleEntity.getRegNr(),
          Option.of(vehicleEntity.getInternalNr()),
          vehicleEntity.getLicensePlateNr(),
          vehicleEntity.getConnectedTypeEntity().getConnectedType(),
          assetTypeConverter.convert(vehicleEntity.getAssetType()),
          // temporal
          Option.none(),
          Collections.emptyList(),
          transformCollection(vehicleEntity.getConnectedVehicles()),
          Option.none(),
          Option.none(),
          Option.none(),
          Option.none()
    );
  }

  public List<VehicleDto> transformCollection(Collection<VehicleEntity> vehicles) {
    if (vehicles == null) {
      return Collections.emptyList();
    }

    return vehicles.stream()
          .map(this::transform)
          .collect(Collectors.toList());
  }
}
