package com.vehco.codriver.server.plugin.mobileoffice.live.dto;

import com.vehco.codriver.server.plugin.mobileoffice.security.LimitedAccess;

import java.util.Collection;
import java.util.Objects;

public final class CurrentVehiclePosition implements LimitedAccess {

  private Long timestamp;
  private Float latitude;
  private Float longitude;
  private Float altitude;
  private Float heading;
  private Float speed;
  private Integer vehicleId;
  private Integer userId;
  private Integer customerId;
  private Collection<Integer> groupIds;

  public CurrentVehiclePosition(
        Long timestamp,
        Float latitude,
        Float longitude,
        Float altitude,
        Float heading,
        Float speed,
        Integer vehicleId,
        Integer userId) {
    this.timestamp = timestamp;
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
    this.heading = heading;
    this.speed = speed;
    this.vehicleId = vehicleId;
    this.userId = userId;
  }

  public CurrentVehiclePosition(CurrentVehiclePosition old) {
    this.timestamp = old.timestamp;
    this.latitude = old.latitude;
    this.longitude = old.longitude;
    this.altitude = old.altitude;
    this.heading = old.heading;
    this.speed = old.speed;
    this.vehicleId = old.vehicleId;
    this.userId = old.userId;
    this.customerId = old.customerId;
    this.groupIds = old.groupIds;
  }

  public void setCustomerId(Integer customerId) {
    this.customerId = customerId;
  }

  public void setGroupIds(Collection<Integer> groupIds) {
    this.groupIds = groupIds;
  }

  public Long getTimestamp() {
    return timestamp;
  }

  public Float getLatitude() {
    return latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public Float getAltitude() {
    return altitude;
  }

  public Float getHeading() {
    return heading;
  }

  public Float getSpeed() {
    return speed;
  }

  public Integer getVehicleId() {
    return vehicleId;
  }

  public Integer getUserId() {
    return userId;
  }

  @Override
  public Integer getCustomerId() {
    return customerId;
  }

  @Override
  public Collection<Integer> getGroupIds() {
    return groupIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CurrentVehiclePosition that = (CurrentVehiclePosition) o;
    return Objects.equals(timestamp, that.timestamp)
          && Objects.equals(vehicleId, that.vehicleId)
          && Objects.equals(userId, that.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, vehicleId, userId);
  }
}
