package com.vehco.codriver.server.plugin.mobileoffice.search.dto;

import com.vehco.codriver.server.plugin.mobileoffice.availability.LoginStatus;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;

public abstract class SearchItem {

  protected Integer id;
  protected LoginStatus loginStatus;
  protected GpsSnapshot position;

  public SearchItem(Integer id, LoginStatus loginStatus, GpsSnapshot position) {
    this.id = id;
    this.loginStatus = loginStatus;
    this.position = position;
  }

  public abstract String getType();

  public Integer getId() {
    return id;
  }

  public LoginStatus getLoginStatus() {
    return loginStatus;
  }

  public GpsSnapshot getPosition() {
    return position;
  }

}
