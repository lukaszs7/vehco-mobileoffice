package com.vehco.codriver.server.plugin.mobileoffice.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "vehicles")
public class VehicleEntity {
  @Id
  @GeneratedValue
  @Column(name = "vehicleid")
  private int vehicleid;

  @Column(name = "customerid")
  private Long customerId;

  @Column(name = "reg_nr")
  private String regNr;

  @Column(name = "internal_nr")
  private String internalNr;

  @Column(name = "body_nr")
  private String bodyNr;

  @Column(name = "type")
  private String type;

  @Column(name = "model_year")
  private Integer modelYear;

  @Column(name = "mobile_nr")
  private String mobileNr;

  @Column(name = "comments")
  private String comments;

  @Column(name = "active")
  private boolean active;

  @Column(name = "license_plate_nr")
  private String licensePlateNr;

  @Column(name = "lastModifiedTimestamp")
  private long lastModifiedTimestamp;

  @Column(name = "trip_meter_offset")
  private Integer tripMeterOffset;

  @Column(name = "access_key")
  private String accessKey;

  @Column(name = "mobile_app_client")
  private Boolean mobileAppClient;

  @Column(name = "origin_id")
  private String originId;

  @Column(name = "rtdLastDownloadTime")
  private Long rtdLastDownloadTime;

  @Column(name = "assetType")
  private String assetType;

  @Column(name = "external_subscription_nr")
  private Integer externalSubscriptionNr;

  @Column(name = "uuid")
  private String uuid;

  @Column(name = "timezone")
  private String timezone;

  @ManyToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "connectedType", referencedColumnName = "id")
  private ConnectedTypeEntity connectedTypeEntity;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "ConnectedVehicles",
        joinColumns = {@JoinColumn(name = "vehicleid")},
        inverseJoinColumns = {@JoinColumn(name = "connectedVehicleid")})
  private Set<VehicleEntity> connectedVehicles = new HashSet<>();
}
