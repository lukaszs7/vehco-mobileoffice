package com.vehco.codriver.server.plugin.mobileoffice.live.dto;

import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import lombok.Value;

@Value
public class LiveElementDto {
  VehicleDetails vehicle;
  User driver;
  GpsSnapshot position;
}
