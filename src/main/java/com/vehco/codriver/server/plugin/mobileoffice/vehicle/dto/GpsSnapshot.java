package com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto;

import lombok.Value;

@Value
public class GpsSnapshot {
  Float latitude;
  Float longitude;
  Float speedKmh;
  Float direction;
  Long timestamp;
  Integer driverId;
}
