package com.vehco.codriver.server.plugin.mobileoffice.security;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiPredicate;

import static java.util.stream.Collectors.toList;

@Component
public class HasAccessPredicate implements BiPredicate<LimitedAccess, AuthMeta> {

  @Override
  public boolean test(LimitedAccess resource, AuthMeta authMeta) {
    List<Integer> commonGroups = resource.getGroupIds().stream()
          .filter(authMeta.getUserGroupIds()::contains)
          .collect(toList());

    return resource.getCustomerId().equals(authMeta.getCustomerId()) && !commonGroups.isEmpty();
  }
}
