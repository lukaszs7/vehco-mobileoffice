package com.vehco.codriver.server.plugin.mobileoffice.live;

import com.vehco.codriver.server.plugin.mobileoffice.live.dto.CurrentVehiclePosition;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.HasAccessPredicate;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;
import java.util.Queue;

public class SseEmitterThread implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(SseEmitterThread.class);

  private AuthMeta authMeta;
  private SseEmitter sseEmitter;
  private LivePositionService livePositionService;
  private HasAccessPredicate hasAccessPredicate;

  public SseEmitterThread(
        AuthMeta authMeta,
        SseEmitter sseEmitter,
        LivePositionService livePositionService,
        HasAccessPredicate hasAccessPredicate) {
    this.authMeta = authMeta;
    this.sseEmitter = sseEmitter;
    this.livePositionService = livePositionService;
    this.hasAccessPredicate = hasAccessPredicate;
  }

  @Override
  public void run() {
    log.debug("Starting new SSE emitter thread...");
    log.debug("Sending data from local queue... ");
    Queue<CurrentVehiclePosition> currentVehiclePositionsQueue = livePositionService.getCachedVehiclePositionsQueue();
    Map<Integer, CurrentVehiclePosition> cachedCurrentPositions = livePositionService.getCachedCurrentPositions();

    CurrentVehiclePosition pos0 = null;
    CurrentVehiclePosition pos1;

    boolean connectionEstablished = true;

    while (connectionEstablished) {
      try {
        pos1 = currentVehiclePositionsQueue.poll();
        if (canEmitLivePosition(pos0, pos1)) {
          emitDataPoint(pos1);
          // free map space
          cachedCurrentPositions.remove(pos1.getVehicleId());
        } else {
          Thread.sleep(100);
        }

        if (pos1 == null) {
          continue;
        }

        pos0 = new CurrentVehiclePosition(pos1);
      } catch (IOException e) {
        log.warn("Error when serving data from queue: ", e);
        connectionEstablished = false;
        sseEmitter.completeWithError(e);
      } catch (InterruptedException e) {
        log.warn("Error when sleeping the thread: ", e);
      }
    }
  }

  private boolean canEmitLivePosition(CurrentVehiclePosition pos0, CurrentVehiclePosition pos1) {
    return pos1 != null && newValueObtained(pos0, pos1) && hasAccessPredicate.test(pos1, authMeta);
  }

  private static boolean newValueObtained(CurrentVehiclePosition previous, CurrentVehiclePosition current) {
    if (previous == null) {
      return true;
    }

    return !previous.equals(current);
  }

  private void emitDataPoint(CurrentVehiclePosition pos) throws IOException {
    GpsSnapshot gps = new GpsSnapshot(
          pos.getLatitude(),
          pos.getLongitude(),
          pos.getSpeed(),
          pos.getHeading(),
          pos.getTimestamp(),
          pos.getUserId());

    SseEmitter.SseEventBuilder event = SseEmitter
          .event()
          .id(pos.getVehicleId().toString())
          .data(livePositionService.buildLiveElementDtoObject(gps, pos.getVehicleId(), pos.getUserId(), authMeta));

    sseEmitter.send(event);
  }

}
