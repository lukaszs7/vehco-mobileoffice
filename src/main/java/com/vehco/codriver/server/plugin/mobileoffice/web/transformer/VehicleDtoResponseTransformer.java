package com.vehco.codriver.server.plugin.mobileoffice.web.transformer;

import com.vehco.codriver.server.plugin.mobileoffice.availability.VehicleAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.VehicleDto;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class VehicleDtoResponseTransformer {

  public List<VehicleDetails> transformCollection(Collection<VehicleDto> vehicles) {
    return vehicles.stream()
          .map(this::transform)
          .collect(Collectors.toList());
  }

  public VehicleDetails transform(VehicleDto vehicleDto) {
    return VehicleDetails.builder()
          .id(vehicleDto.getId())
          .assetType(vehicleDto.getAssetType())
          .availability(vehicleDto.getAvailability()
                .map(availability -> new VehicleAvailability(null, availability.getLoginTs(),
                      availability.getLogoutTs())).getOrNull())
          .connected(transformCollection(vehicleDto.getConnectedVehicles()))
          .connectedType(vehicleDto.getConnectedType())
          .customerId(null)
          .groupIds(null)
          .internalNr(vehicleDto.getInternalNr().getOrNull())
          .lastKnownMileage(null)
          .lastKnownTachographState(vehicleDto.getLastKnownTachographState().getOrNull())
          .licensePlateNr(vehicleDto.getLicensePlateNr())
          .position(null)
          .roadBoxId(vehicleDto.getRoadBoxId())
          .sensors(null)
          .build();
  }
}
