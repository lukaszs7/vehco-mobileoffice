package com.vehco.codriver.server.plugin.mobileoffice.web.transformer;

import com.vehco.codriver.server.common.avro.position.AvroPosition;
import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.dto.GpsSnapshot;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import com.vehco.codriver.server.plugin.mobileoffice.web.dto.LivePositionResponseDto;
import com.vehco.codriver.server.plugin.mobileoffice.web.dto.UserResponseDto;
import com.vehco.codriver.server.plugin.mobileoffice.web.dto.VehicleResponseDto;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

import static io.vavr.control.Option.of;

@Component
@RequiredArgsConstructor
public class LiveDtoResponseTransformer {
  private final GpsPositionResponseTransformer gpsPositionResponseTransformer;

  public LivePositionResponseDto transform(AvroPosition position, Option<VehicleDetails> optionVehicle,
                                           Option<User> userDetails) {
    return LivePositionResponseDto.builder()
          .position(gpsPositionResponseTransformer.transform(position))
          .driver(userDetails.map(this::transformDriver))
          .vehicle(optionVehicle.map(this::transformVehicle))
          .build();
  }

  public LivePositionResponseDto transform(GpsSnapshot position, Option<VehicleDetails> optionVehicle,
                                           Option<User> userDetails) {
    return LivePositionResponseDto.builder()
          .position(gpsPositionResponseTransformer.transform(position))
          .driver(userDetails.map(this::transformDriver))
          .vehicle(optionVehicle.map(this::transformVehicle))
          .build();
  }

  // TODO temporal mapping to have backwards compability
  private VehicleResponseDto transformVehicle(VehicleDetails vehicleDetails) {
    return VehicleResponseDto.builder()
          .id(vehicleDetails.getId())
          .assetType(vehicleDetails.getAssetType())
          .availability(Option.of(vehicleDetails.getAvailability()))
          .connectedType(vehicleDetails.getConnectedType())
          .internalNr(vehicleDetails.getInternalNr())
          .lastKnownTachographState(Option.of(vehicleDetails.getLastKnownTachographState()))
          .connected(List.ofAll(
                vehicleDetails.getConnected().stream().map(this::transformVehicle).collect(Collectors.toList())))
          .build();
  }

  // TODO temporal mapping to have backwards compability
  private UserResponseDto transformDriver(User user) {
    return UserResponseDto.builder()
          .id(user.getId())
          .lastName(of(user.getLastName()))
          .surName(of(user.getSurName()))
          .userName(user.getUserName())
          .email(of(user.getEmail()))
          .build();
  }
}
