package com.vehco.codriver.server.plugin.mobileoffice.security;

public final class Permissions {

  // Spring security config requires that permissions are prefixed with ROLE_
  public static final String MOBILE_OFFICE_BASIC = "ROLE_OFFICE_BASIC_MOBILE";

}
