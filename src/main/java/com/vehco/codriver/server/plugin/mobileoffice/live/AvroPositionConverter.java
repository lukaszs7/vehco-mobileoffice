package com.vehco.codriver.server.plugin.mobileoffice.live;

import com.vehco.codriver.server.common.avro.position.AvroPosition;
import com.vehco.codriver.server.plugin.mobileoffice.live.dto.CurrentVehiclePosition;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AvroPositionConverter implements Converter<AvroPosition, CurrentVehiclePosition> {

  @Override
  public CurrentVehiclePosition convert(AvroPosition avro) {
    Integer vehicleId = avro.getVehicleId();

    return new CurrentVehiclePosition(
          avro.getTimestamp(),
          avro.getLatitude(),
          avro.getLongitude(),
          avro.getAltitude(),
          avro.getHeading(),
          avro.getSpeed(),
          vehicleId,
          avro.getUserId());
  }
}
