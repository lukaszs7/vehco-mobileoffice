package com.vehco.codriver.server.plugin.mobileoffice.availability;

import lombok.Getter;

@Getter
public class Availability {

  private LoginStatus status;
  private Long loginTs;
  private Long logoutTs;

  public Availability(Long loginTs, Long logoutTs) {
    this.loginTs = loginTs;
    this.logoutTs = logoutTs;
    this.status = determineLoginStatus(loginTs, logoutTs);
  }

  protected LoginStatus determineLoginStatus(Long loginTs, Long logoutTs) {
    LoginStatus loginStatus = LoginStatus.UNKNOWN;

    if (loginTs == null && logoutTs == null) {
      loginStatus = LoginStatus.NEVER_LOGGED_IN;
    }

    if (loginTs != null) {
      if (logoutTs == null) {
        loginStatus = LoginStatus.LOGGED_IN;
      } else {
        loginStatus = LoginStatus.LOGGED_OUT;
      }
    }

    return loginStatus;
  }

}
