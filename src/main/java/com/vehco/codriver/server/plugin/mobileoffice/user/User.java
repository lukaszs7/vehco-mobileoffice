package com.vehco.codriver.server.plugin.mobileoffice.user;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vehco.codriver.server.plugin.mobileoffice.availability.UserAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.security.LimitedAccess;

import java.util.Collection;

public class User implements LimitedAccess {

  private Integer id;
  private String lastName;
  private String surName;
  private String userName;
  private String phoneNumber;
  private String email;
  private UserAvailability availability;

  @JsonIgnore
  private Integer customerId;

  @JsonIgnore
  private Collection<Integer> groupIds;

  public User(User other) {
    this.id = other.id;
    this.lastName = other.lastName;
    this.surName = other.surName;
    this.userName = other.userName;
    this.phoneNumber = other.phoneNumber;
    this.email = other.email;
    this.customerId = other.customerId;
    this.availability = other.availability;
  }

  public User(
        Integer id,
        String lastName,
        String surName,
        String userName,
        String phoneNumber,
        String email,
        Integer customerId) {
    this.id = id;
    this.lastName = lastName;
    this.surName = surName;
    this.userName = userName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.customerId = customerId;
  }

  public Integer getId() {
    return id;
  }

  public String getLastName() {
    return lastName;
  }

  public String getSurName() {
    return surName;
  }

  public String getUserName() {
    return userName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public Integer getCustomerId() {
    return customerId;
  }

  public UserAvailability getAvailability() {
    return availability;
  }

  public void setAvailability(UserAvailability availability) {
    this.availability = availability;
  }

  public Collection<Integer> getGroupIds() {
    return groupIds;
  }

  public void setGroupIds(Collection<Integer> groupIds) {
    this.groupIds = groupIds;
  }
}
