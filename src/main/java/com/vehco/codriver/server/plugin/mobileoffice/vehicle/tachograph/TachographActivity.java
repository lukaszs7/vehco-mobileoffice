package com.vehco.codriver.server.plugin.mobileoffice.vehicle.tachograph;

public enum TachographActivity {
  DRIVING, WORKING, RESTING, AVAILABLE, UNKNOWN
}
