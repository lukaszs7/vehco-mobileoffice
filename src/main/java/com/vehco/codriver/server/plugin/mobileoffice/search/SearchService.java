package com.vehco.codriver.server.plugin.mobileoffice.search;

import com.vehco.codriver.server.plugin.mobileoffice.AbstractService;
import com.vehco.codriver.server.plugin.mobileoffice.availability.Availability;
import com.vehco.codriver.server.plugin.mobileoffice.availability.UserAvailability;
import com.vehco.codriver.server.plugin.mobileoffice.search.dto.DriverItem;
import com.vehco.codriver.server.plugin.mobileoffice.search.dto.SearchItem;
import com.vehco.codriver.server.plugin.mobileoffice.search.dto.VehicleItem;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.HasAccessPredicate;
import com.vehco.codriver.server.plugin.mobileoffice.user.User;
import com.vehco.codriver.server.plugin.mobileoffice.user.UserService;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.VehicleService;
import com.vehco.codriver.server.plugin.mobileoffice.vehicle.web.dto.VehicleDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.vehco.codriver.server.plugin.mobileoffice.availability.LoginStatus.UNKNOWN;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class SearchService extends AbstractService {

  private final UserService userService;
  private final VehicleService vehicleService;
  private final HasAccessPredicate hasAccessPredicate;

  public Collection<SearchItem> search(String query, AuthMeta authMeta) {
    String queryLc = query.toLowerCase();

    return Stream
          .concat(
                findMatchingDrivers(queryLc, authMeta),
                findMatchingVehicles(queryLc, authMeta))
          .sorted(comparing(SearchItem::getType))
          .collect(toList());
  }

  private Stream<DriverItem> findMatchingDrivers(String query, AuthMeta authMeta) {
    List<Predicate<User>> criteria = new ArrayList<>();
    criteria.add(d -> d.getUserName().toLowerCase().contains(query));
    criteria.add(d -> d.getSurName() != null && d.getSurName().toLowerCase().contains(query));
    criteria.add(d -> d.getLastName() != null && d.getLastName().toLowerCase().contains(query));

    Map<Integer, User> allUsers = userService.findAllUsers(authMeta);

    return allUsers.values().stream()
          .filter(u -> hasAccessPredicate.test(u, authMeta))
          .filter(criteria.stream().reduce(x -> false, Predicate::or))
          .map(u -> new DriverItem(
                u.getId(),
                Optional.ofNullable(u.getAvailability())
                      .map(Availability::getStatus)
                      .orElse(UNKNOWN),
                Optional.ofNullable(u.getAvailability())
                      .map(UserAvailability::getVehicleId)
                      .map(livePositionService::getVehicleGpsPosition)
                      .orElse(null),
                u.getSurName(),
                u.getLastName(),
                u.getUserName()));
  }

  private Stream<VehicleItem> findMatchingVehicles(String query, AuthMeta authMeta) {
    List<Predicate<VehicleDetails>> criteria = new ArrayList<>();
    criteria.add(v -> v.getRoadBoxId() != null && v.getRoadBoxId().toLowerCase().contains(query));
    criteria.add(v -> v.getInternalNr() != null && v.getInternalNr().toLowerCase().contains(query));
    criteria.add(v -> v.getLicensePlateNr() != null && v.getLicensePlateNr().toLowerCase().contains(query));

    return vehicleService.getAllVehicleDetails(authMeta).values().stream()
          .filter(vd -> hasAccessPredicate.test(vd, authMeta))
          .filter(criteria.stream().reduce(x -> false, Predicate::or))
          .map(v -> new VehicleItem(
                v.getId(),
                Optional.ofNullable(v.getAvailability())
                      .map(Availability::getStatus)
                      .orElse(UNKNOWN),
                livePositionService.getVehicleGpsPosition(v.getId()),
                v.getRoadBoxId(),
                v.getInternalNr(),
                v.getLicensePlateNr(),
                v.getAssetType()));
  }

}
