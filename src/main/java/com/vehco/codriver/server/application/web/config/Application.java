package com.vehco.codriver.server.application.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value = {
      com.vehco.codriver.server.persistence.config.CodriverDefaultPersistenceConfig.class,
      com.vehco.codriver.server.office.session.ThreadLocalWebOfficeSessionContextHolder.class,
      com.vehco.codriver.server.services.admin.persist.UserDAOImpl.class,
      com.vehco.codriver.server.services.admin.persist.GroupDAOImpl.class,
      com.vehco.codriver.server.services.admin.persist.CustomerDAOImpl.class
})
@ComponentScan(basePackages = {
      "com.vehco.codriver.server.application",
      "com.vehco.codriver.server.plugin.mobileoffice",
      "com.vehco.codriver.server.auth.jwt"
})
public class Application {
  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  public static void main(final String[] args) {
    LOGGER.info("Application starting...");
    SpringApplication.run(Application.class, args);
    LOGGER.info("Application started.");
  }
}
