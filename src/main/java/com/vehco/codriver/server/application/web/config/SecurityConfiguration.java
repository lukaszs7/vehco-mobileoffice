package com.vehco.codriver.server.application.web.config;

import com.vehco.codriver.server.application.web.config.filters.WebofficeSessionContextInjectionFilter;
import com.vehco.codriver.server.auth.jwt.JwtAuthorizationFilter;
import com.vehco.codriver.server.auth.jwt.UserLoader;
import com.vehco.codriver.server.office.WebOfficeSessionContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SecurityConfiguration {

  @Configuration
  public static class WebOfficeSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserLoader userLoader;

    @Autowired
    WebOfficeSessionContextHolder webOfficeSessionContextHolder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http /*.cors().and()*/
            .csrf().disable()
            .antMatcher("/**")
            .authorizeRequests()
              .antMatchers("/actuator/**").permitAll()
              .antMatchers("/webofficeexternalrpc/mobileoffice/**").authenticated()
            .anyRequest().denyAll()
            .and()
            .addFilter(getApplicationContext().getBean(JwtAuthorizationFilter.class))
            .addFilterAfter(
                  new WebofficeSessionContextInjectionFilter(webOfficeSessionContextHolder),
                  SecurityContextHolderAwareRequestFilter.class)
            .sessionManagement()
            .sessionFixation().none()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public JwtAuthorizationFilter provideWebofficeJwtAuthorizationFilter() throws Exception {
      return new JwtAuthorizationFilter(authenticationManagerBean());
    }
  }
}
