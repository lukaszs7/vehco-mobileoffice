package com.vehco.codriver.server.application.web.config.filters;

import com.vehco.codriver.server.auth.jwt.SessionUserValidator;
import com.vehco.codriver.server.office.WebOfficeSessionContext;
import com.vehco.codriver.server.office.WebOfficeSessionContextHolder;
import com.vehco.codriver.server.office.session.ThreadLocalWebOfficeSessionContextHolder;
import com.vehco.codriver.server.services.admin.persist.PersistentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class WebofficeSessionContextInjectionFilter extends OncePerRequestFilter {
  private static final Logger log = LoggerFactory.getLogger(WebofficeSessionContextInjectionFilter.class);
  private WebOfficeSessionContextHolder webOfficeSessionContextHolder;

  public WebofficeSessionContextInjectionFilter(WebOfficeSessionContextHolder webOfficeSessionContextHolder) {
    this.webOfficeSessionContextHolder = webOfficeSessionContextHolder;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain filterChain) throws IOException, ServletException {
    try {
      log.trace("User principal: " + request.getUserPrincipal());

      if (request.getUserPrincipal() != null) {
        final HttpSession session = request.getSession();

        PersistentUser persistentUser = new SessionUserValidator(session).getSessionUser();

        ((ThreadLocalWebOfficeSessionContextHolder) webOfficeSessionContextHolder).setCurrentSessionContext(
              new WebOfficeSessionContext(persistentUser, request.getSession()));
      }

      filterChain.doFilter(request, response);
    } finally {
      ((ThreadLocalWebOfficeSessionContextHolder) webOfficeSessionContextHolder).reset();
    }
  }
}
