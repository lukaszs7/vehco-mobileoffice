package com.vehco.codriver.server.office.util;

import com.vehco.codriver.server.office.WebOfficeSessionContext;
import com.vehco.codriver.server.services.admin.persist.PersistentGroup;
import com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Set;

import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_ALLGROUPS;
import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_GROUPSMEMBER;
import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_SELFONLY;

public class DataAccessRestrictionUtil {
  public static DataPermissionType getDataAccessPermission() {
    DataPermissionType type = null;
    if (hasPermission(USER_ALLGROUPS.name())) {
      type = USER_ALLGROUPS;
    } else if (hasPermission(USER_GROUPSMEMBER.name())) {
      type = USER_GROUPSMEMBER;
    } else if (hasPermission(USER_SELFONLY.name())) {
      type = USER_SELFONLY;
    } else {
      //No access permission is set, default to most restrictive
      type = USER_SELFONLY;
    }
    return type;
  }


  private static boolean hasPermission(String permission) {
    return SecurityContextHolder.getContext().getAuthentication()
          .getAuthorities().contains(new SimpleGrantedAuthority("ROLE_" + permission));
  }

  public static void addGroupIdsRecursively(final Set<PersistentGroup> groups, final Set<Integer> permittedGroupIds) {
    for (PersistentGroup group : groups) {
      if (!permittedGroupIds.contains(group.getId())) {
        permittedGroupIds.add(group.getId());
        Set<PersistentGroup> children = group.getChildren();
        DataAccessRestrictionUtil.addGroupIdsRecursively(children, permittedGroupIds);
      }
    }
  }

  public static void addGroupRecursively(
        final Set<PersistentGroup> groups, final Set<PersistentGroup> permittedGroups) {
    for (PersistentGroup group : groups) {
      if (!permittedGroups.contains(group)) {
        permittedGroups.add(group);
        Set<PersistentGroup> children = group.getChildren();
        DataAccessRestrictionUtil.addGroupRecursively(children, permittedGroups);
      }
    }
  }

  public static DataAccessRestriction getSessionCachedAccessRestriction(
        WebOfficeSessionContext sessionContext, boolean useHierarchicalGroupStructure) {
    DataAccessRestriction accessRestriction = (DataAccessRestriction) sessionContext.getHttpSession()
          .getAttribute("dataAccessRestriction");

    if (accessRestriction == null || !accessRestriction.getUser().equals(sessionContext.getLoggedInUser())) {
      accessRestriction = new DataAccessRestriction(sessionContext.getLoggedInUser(), useHierarchicalGroupStructure);
      sessionContext.getHttpSession().setAttribute("dataAccessRestriction", accessRestriction);
    }
    return accessRestriction;
  }
}
