package com.vehco.codriver.server.office;

import com.vehco.codriver.server.services.admin.persist.PersistentUser;

import javax.servlet.http.HttpSession;

public class WebOfficeSessionContext {
  private PersistentUser persistentUser;
  private HttpSession httpSession;

  public WebOfficeSessionContext(PersistentUser persistentUser, HttpSession httpSession) {
    this.persistentUser = persistentUser;
    this.httpSession = httpSession;
  }

  public PersistentUser getLoggedInUser() {
    return persistentUser;
  }

  public HttpSession getHttpSession() {
    return httpSession;
  }
}
