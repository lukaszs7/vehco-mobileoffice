package com.vehco.codriver.server.office;

import com.vehco.codriver.server.office.session.ThreadLocalWebOfficeSessionContextHolder;

/**
 * Holder for the current {@link WebOfficeSessionContext} that is associated
 * with the current weboffice request. An instance extending this class (see
 * {@link ThreadLocalWebOfficeSessionContextHolder}) is available in the spring
 * context.
 *
 * @author danmor
 */
public abstract class WebOfficeSessionContextHolder {

  public abstract WebOfficeSessionContext getCurrentSessionContext();

}
