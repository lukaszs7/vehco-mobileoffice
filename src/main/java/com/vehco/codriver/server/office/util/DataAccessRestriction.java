package com.vehco.codriver.server.office.util;

import com.vehco.codriver.foundation.xml.admin.Customer;
import com.vehco.codriver.server.services.admin.persist.PersistentGroup;
import com.vehco.codriver.server.services.admin.persist.PersistentUser;
import com.vehco.codriver.server.services.admin.persist.PersistentVehicle;
import com.vehco.codriver.server.services.admin.persist.UserSearchCriteria;
import com.vehco.codriver.server.services.admin.persist.VehicleSearchCriteria;
import com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_ALLGROUPS;
import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_GROUPSMEMBER;
import static com.vehco.codriver.weboffice.shared.authentication.DataPermission.DataPermissionType.USER_SELFONLY;

public class DataAccessRestriction {

  private PersistentUser user;
  private DataPermissionType dataAccessPermission;
  private boolean useHierarchicalGroupStructure;
  private Set<Integer> cachedHierarchicalPermittedGroupIds;
  private Object cacheLock = new Object();

  public DataAccessRestriction(PersistentUser user, boolean useHierarchicalGroupStructure) {
    this.user = user;
    dataAccessPermission = DataAccessRestrictionUtil.getDataAccessPermission();
    this.useHierarchicalGroupStructure = useHierarchicalGroupStructure;
  }

  public void apply(VehicleSearchCriteria sc) {
    if (dataAccessPermission == USER_GROUPSMEMBER
          || (dataAccessPermission == USER_ALLGROUPS && useHierarchicalGroupStructure)) {
      sc.setGroupIds(restrictGroupIds(sc.getGroupIds()));
    } else if (dataAccessPermission == USER_SELFONLY) {
      sc.setLastLoggedInUserId(user.getId());
    }
  }

  // used if flat group structure
  public void apply(Collection<PersistentGroup> groups) {
    if (dataAccessPermission == DataPermissionType.USER_GROUPSMEMBER
          || dataAccessPermission == DataPermissionType.USER_SELFONLY) {
      Set<Integer> permittedGroupIds = toIdSet(user.getGroups());
      for (Iterator<PersistentGroup> it = groups.iterator(); it.hasNext(); ) {
        if (!permittedGroupIds.contains(it.next().getId())) {
          it.remove();
        }
      }
    }
  }

  public void apply(UserSearchCriteria sc) {
    if (dataAccessPermission == USER_GROUPSMEMBER
          || (dataAccessPermission == USER_ALLGROUPS && useHierarchicalGroupStructure)) {
      sc.setGroupIds(restrictGroupIds(sc.getGroupIds()));
    }
  }

  // used if hierarchical group structure
  public AccessControlledPersistentGroup apply(PersistentGroup rootGroup) {
    if (dataAccessPermission == USER_GROUPSMEMBER
          || dataAccessPermission == USER_SELFONLY
          || dataAccessPermission == USER_ALLGROUPS) {
      return recursiveApply(toIdSet(user.getGroups()), rootGroup);
    }

    return new AccessControlledPersistentGroup(rootGroup);
  }

  /**
   * Returns the set of group-ids that the user is allowed to access.
   */
  public Set<Integer> getPermittedGroupIds() {
    Set<Integer> permittedGroupIds;
    if (dataAccessPermission == USER_SELFONLY) {
      permittedGroupIds = Collections.emptySet();
    } else if (dataAccessPermission == USER_ALLGROUPS && useHierarchicalGroupStructure) {
      permittedGroupIds = getPermittedGroupIdsHierarchicalGroupStructure();
    } else {
      permittedGroupIds = toIdSet(user.getGroups());
    }
    return permittedGroupIds;
  }

  private AccessControlledPersistentGroup recursiveApply(Set<Integer> permittedGroupIds, PersistentGroup group) {
    AccessControlledPersistentGroup accessControlledGroup = new AccessControlledPersistentGroup(group);
    boolean accessible = permittedGroupIds.contains(group.getId());
    accessControlledGroup.setAccessible(accessible);

    Set<PersistentGroup> children = group.getChildren();
    HashSet<AccessControlledPersistentGroup> controlledChildren = new HashSet<AccessControlledPersistentGroup>();

    if (needToCheckAccessToChildren(accessible)) {
      for (PersistentGroup child : children) {
        controlledChildren.add(recursiveApply(permittedGroupIds, child));
      }

      accessControlledGroup.setAccessControlledChildren(controlledChildren);
    }

    return accessControlledGroup;
  }

  private boolean needToCheckAccessToChildren(boolean parentAccessible) {
    boolean needToCheck = !parentAccessible
          || dataAccessPermission == USER_GROUPSMEMBER
          || dataAccessPermission == USER_SELFONLY;

    return needToCheck;
  }

  private Set<Integer> restrictGroupIds(Set<Integer> groupIds) {
    Set<Integer> permittedGroupIds = null;

    if (useHierarchicalGroupStructure && dataAccessPermission == USER_ALLGROUPS) {
      permittedGroupIds = getPermittedGroupIdsHierarchicalGroupStructure();
    } else {
      permittedGroupIds = toIdSet(user.getGroups());
    }

    if (groupIds == null || groupIds.isEmpty()) {
      // restrict groups to set of  permitted
      groupIds = permittedGroupIds;
    } else {
      //remove all selected groups that is not also in the set of permitted groups
      groupIds.retainAll(permittedGroupIds);
      if (groupIds.isEmpty()) {
        groupIds.add(-1);
      }
    }

    return groupIds;
  }

  private Set<Integer> getPermittedGroupIdsHierarchicalGroupStructure() {
    synchronized (cacheLock) {
      if (cachedHierarchicalPermittedGroupIds == null) {
        cachedHierarchicalPermittedGroupIds = new HashSet<Integer>();
        DataAccessRestrictionUtil.addGroupIdsRecursively(user.getGroups(), cachedHierarchicalPermittedGroupIds);
      }
      return cachedHierarchicalPermittedGroupIds;
    }
  }

  private static Set<Integer> toIdSet(Set<PersistentGroup> groups) {
    Set<Integer> permittedGroupIds = new HashSet<Integer>();
    for (PersistentGroup persistent : groups) {
      permittedGroupIds.add(persistent.getId());
    }
    return permittedGroupIds;
  }

  public DataPermissionType getDataAccessPermission() {
    return dataAccessPermission;
  }

  public static class AccessControlledPersistentGroup extends PersistentGroup {
    private PersistentGroup delegate;
    private boolean accessible = true;
    private HashSet<AccessControlledPersistentGroup> controlledChildren;

    public AccessControlledPersistentGroup(PersistentGroup delegate) {
      this.delegate = delegate;
    }

    public void setAccessControlledChildren(HashSet<AccessControlledPersistentGroup> controlledChildren) {
      this.controlledChildren = controlledChildren;
    }

    public Set<AccessControlledPersistentGroup> getAccessControlledChildren() {
      if (controlledChildren == null) {
        controlledChildren = new HashSet<>();
        for (PersistentGroup child : getChildren()) {
          controlledChildren.add(new AccessControlledPersistentGroup(child));
        }
      }
      return controlledChildren;
    }

    public boolean isAccessible() {
      return accessible;
    }

    public void setAccessible(boolean accessible) {
      this.accessible = accessible;
    }

    @Override
    public int getId() {
      return delegate.getId();
    }

    @Override
    public String getName() {
      return delegate.getName();
    }

    @Override
    public Customer getCustomer() {
      return delegate.getCustomer();
    }

    @Override
    public Set<PersistentUser> getUsers() {
      return delegate.getUsers();
    }

    @Override
    public Set<PersistentVehicle> getVehicles() {
      return delegate.getVehicles();
    }

    @Override
    public Set<PersistentGroup> getChildren() {
      return delegate.getChildren();
    }

    @Override
    public Long getLastModified() {
      return delegate.getLastModified();
    }

    @Override
    public PersistentGroup getParent() {
      return delegate.getParent();
    }

    @Override
    public boolean isAutocreated() {
      return delegate.isAutocreated();
    }

    @Override
    public boolean isVisible() {
      return delegate.isVisible();
    }

    @Override
    public String toString() {
      return delegate.toString();
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((delegate == null) ? 0 : delegate.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!super.equals(obj)) {
        return false;
      }
      if (!(obj instanceof AccessControlledPersistentGroup)) {
        return false;
      }
      AccessControlledPersistentGroup other = (AccessControlledPersistentGroup) obj;
      if (delegate == null) {
        if (other.delegate != null) {
          return false;
        }
      } else if (!delegate.equals(other.delegate)) {
        return false;
      }
      return true;
    }
  }

  public PersistentUser getUser() {
    return user;
  }
}
