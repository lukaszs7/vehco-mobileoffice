package com.vehco.codriver.server.office.session;

import com.vehco.codriver.server.office.WebOfficeSessionContext;
import com.vehco.codriver.server.office.WebOfficeSessionContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ThreadLocalWebOfficeSessionContextHolder extends WebOfficeSessionContextHolder {

  private static ThreadLocal<WebOfficeSessionContext> sessionThreadLocal = new ThreadLocal<>();

  ThreadLocalWebOfficeSessionContextHolder() {
  }

  public void reset() {
    sessionThreadLocal.remove();
  }

  public void setCurrentSessionContext(WebOfficeSessionContext sessionContext) {
    sessionThreadLocal.set(sessionContext);
  }

  @Override
  public WebOfficeSessionContext getCurrentSessionContext() {
    return sessionThreadLocal.get();
  }

}
