package com.vehco.codriver.server.security;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = MockUserSecurityContextFactory.class)
public @interface WithVehcoUser {

  String userName() default "admin";

  String customerName() default "vehco";

  String[] permissions();

}
