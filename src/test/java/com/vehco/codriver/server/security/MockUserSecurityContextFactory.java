package com.vehco.codriver.server.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Arrays;
import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class MockUserSecurityContextFactory implements WithSecurityContextFactory<WithVehcoUser> {

  @Override
  public SecurityContext createSecurityContext(WithVehcoUser annotation) {
    SecurityContext sc = SecurityContextHolder.createEmptyContext();

    Authentication auth = new UsernamePasswordAuthenticationToken(
          annotation.customerName() + "/" + annotation.userName(),
          null,
          toAuthorities(annotation.permissions()));

    sc.setAuthentication(auth);

    return sc;
  }

  private static Collection<? extends GrantedAuthority> toAuthorities(String[] permissions) {
    return Arrays.stream(permissions)
          .map(SimpleGrantedAuthority::new)
          .collect(toList());
  }

}
