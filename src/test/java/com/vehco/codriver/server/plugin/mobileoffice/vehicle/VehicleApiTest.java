package com.vehco.codriver.server.plugin.mobileoffice.vehicle;

import com.vehco.codriver.server.plugin.mobileoffice.ControllerExceptionHandler;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;
import com.vehco.codriver.server.plugin.mobileoffice.security.Permissions;
import com.vehco.codriver.server.security.WithVehcoUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = {VehicleApi.class, ControllerExceptionHandler.class})
public class VehicleApiTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private AuthMetaExtractor authMetaExtractor;

  @MockBean
  private VehicleService service;

  @Test
  @WithVehcoUser(permissions = Permissions.MOBILE_OFFICE_BASIC)
  void shouldCallService() throws Exception {
    // given
    Integer vehicleId = 1;
    AuthMeta auth = new AuthMeta(1, 2, null);
    when(authMetaExtractor.fromSession()).thenReturn(auth);
    when(service.findOne(vehicleId, auth)).thenReturn(Optional.empty());

    // when
    mockMvc
          .perform(
                get("/webofficeexternalrpc/mobileoffice/vehicle/{vehicleId}", vehicleId))
          .andExpect(status().isNotFound());

    // then
    verify(service).findOne(vehicleId, auth);
  }

}
