package com.vehco.codriver.server.plugin.mobileoffice.live;

import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMeta;
import com.vehco.codriver.server.plugin.mobileoffice.security.AuthMetaExtractor;
import com.vehco.codriver.server.plugin.mobileoffice.security.Permissions;
import com.vehco.codriver.server.security.WithVehcoUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(classes = {LiveVehicleApi.class})
public class LiveVehicleApiTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private AuthMetaExtractor authMetaExtractor;

  @MockBean
  private LivePositionService service;

//  @Test
//  @WithVehcoUser(permissions = Permissions.MOBILE_OFFICE_BASIC)
//  void shouldCallService() throws Exception {
//    // given
//    AuthMeta auth = new AuthMeta(1, 2, null);
//    when(authMetaExtractor.fromSession()).thenReturn(auth);
//    when(service.getCurrentPositions(auth)).thenReturn(emptyList());
//
//    // when
//    mockMvc
//          .perform(get("/webofficeexternalrpc/mobileoffice/live"))
//          .andExpect(status().isOk());
//
//    // then
//    verify(service).getCurrentPositions(auth);
//  }

}
