FROM docker.codriver.com/base/openjdk:11.0.9-jre-slim-vehco-68

VOLUME /tmp
RUN chmod -R g+rwX /tmp

# Install commands to image
RUN apt-get update && \
    apt-get install -y \
    curl \
    dumb-init \
    unzip \
    tzdata

# Install scripts to image
COPY target/startup-scripts/server-common-startup-scripts*.zip /vehco/startup-scripts/startup-scripts.zip
RUN unzip /vehco/startup-scripts/startup-scripts.zip -d /vehco/startup-scripts && \
    rm /vehco/startup-scripts/startup-scripts.zip

# Add SpringBoot application jar to image
ARG JAR_FILE
ADD ${JAR_FILE} /vehco/app/app.jar

ARG JAVA_OPTS
ENV JAVA_OPTS=$JAVA_OPTS

ENV PATH=$PATH:/vehco/startup-scripts
ENV TZ=Europe/Stockholm

ARG BUNDLE_VERSION
ENV INFO_APP_BUNDLE_VERSION=${BUNDLE_VERSION}

WORKDIR /vehco/app

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["wait-for-db-upgrade.sh", "java", "-Xms32m", "-Xmx1536m", "-jar", "app.jar"]








